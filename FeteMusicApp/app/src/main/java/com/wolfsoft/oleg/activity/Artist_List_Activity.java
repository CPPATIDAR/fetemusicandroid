package com.wolfsoft.oleg.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.adapters.Artist_List_Detail_Adapter;
import com.wolfsoft.oleg.adapters.Play_List_Detail_Adapter;
import com.wolfsoft.oleg.bean.ArtistData;
import com.wolfsoft.oleg.bean.FeaturedResponse;
import com.wolfsoft.oleg.bean.Featured_Audio_Data;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;
import com.wolfsoft.oleg.utils.NetworkConnection;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Artist_List_Activity extends AppCompatActivity {

    Context context;
    RecyclerView recyclerViewPlayList;
    ArrayList<Featured_Audio_Data> listArtistData;
    Artist_List_Detail_Adapter adapter;

    String artist_id,type = "audio";
    int page_no = 1;
    String header;
    ImageView imageView_Back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_list);

        context = Artist_List_Activity.this;
        header  = ConstantMember.getHeader(context);

        initComponent();
        initListener();

        Intent intent = getIntent();
        artist_id = ""+intent.getStringExtra("artist_id");
        Log.e("playlist "," id= "+artist_id);

        if (NetworkConnection.checkConnection(context)) {
            getArtistList();
        } else {
            ConstantMember.showMessage(context,"No internet connection");
        }
    }

    public void initComponent() {

        imageView_Back = findViewById(R.id.image_view_back);

        recyclerViewPlayList = findViewById(R.id.recycler);
        recyclerViewPlayList.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

    }

    public void initListener() {
        imageView_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void getArtistList() {
        DialogsUtils.showProgressDialog(context,"Please wait...");

        Api.getClient().getMedia(header,type,"0",""+artist_id,""+page_no).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body().isStatus()) {

                        DialogsUtils.dismissDialog();

                        Log.e("search ","response = "+response.body().getMediaList());

                        listArtistData    = response.body().getMediaList();


                        callAdapter();

                    }
                } else {
                    DialogsUtils.dismissDialog();
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                DialogsUtils.dismissDialog();
            }
        });
    }

    public void callAdapter() {
        if (listArtistData.size() > 0) {
            adapter = new Artist_List_Detail_Adapter(context,listArtistData);
            recyclerViewPlayList.setAdapter(adapter);
        }
    }
}
