package com.wolfsoft.oleg.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.jean.jcplayer.JcPlayerManagerListener;
import com.example.jean.jcplayer.general.JcStatus;
import com.example.jean.jcplayer.general.errors.OnInvalidPathListener;
import com.example.jean.jcplayer.model.JcAudio;
import com.example.jean.jcplayer.view.JcPlayerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.bean.Featured_Audio_Data;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class Audio_Player_Activity extends AppCompatActivity implements View.OnClickListener,OnInvalidPathListener, JcPlayerManagerListener,SeekBar.OnSeekBarChangeListener{

    Activity activity;
    int position = 0,total_lenth,media_id =0;
    String str_audio_list,str_image_path,str_audio_path,str_title,str_commingfrom,header;
    List<Featured_Audio_Data> audio_dataList = new ArrayList<>();
    List<Featured_Audio_Data> audio_dataList2;
    Gson gson = new Gson();

    ImageView iv_audio_img,iv_play,iv_pause,iv_previous,iv_next,iv_fav,iv_fav_select,iv_repeat,iv_ad_to_list;
    TextView tv_title,tv_sub_title,tv_start_time,tv_end_time;
    LinearLayout linear_back,linear_search;
    SeekBar seekBar;
    ProgressBar progressBar;

    private JcPlayerView player;
    ArrayList<JcAudio> jcAudios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio__player);
        activity = Audio_Player_Activity.this;
        header = ConstantMember.getHeader(activity);
        initComponent();

        Intent intent = getIntent();
        if (intent != null) {
            position = intent.getIntExtra("position",0);
            str_commingfrom = intent.getStringExtra("comming_from");
            if(str_commingfrom.equals("home")){
                str_audio_list = ConstantMember.getValueFromSession(activity,"featured_audio_list");
                if (!str_audio_list.equals("")) {
                    audio_dataList =  gson.fromJson(str_audio_list, new TypeToken<List<Featured_Audio_Data>>(){}.getType());
                    media_id = audio_dataList.get(position).getId();
                }
            }else {
                str_audio_list = ConstantMember.getValueFromSession(activity,"user_playlist");
                if (!str_audio_list.equals("")) {
                    audio_dataList =  gson.fromJson(str_audio_list, new TypeToken<List<Featured_Audio_Data>>(){}.getType());
                    media_id = audio_dataList.get(position).getId();
                }
            }
                if (audio_dataList.size()>0) {
                    total_lenth = audio_dataList.size();
                    str_title = audio_dataList.get(position).getTitle();
                    str_image_path = audio_dataList.get(position).getFile_image();
                    loadAudioImage(str_image_path,str_title);
                    str_audio_path = audio_dataList.get(position).getFile_path();

                    jcAudios = new ArrayList<>();
                    for (int i =0; i <audio_dataList.size(); i++) {
                        jcAudios.add(JcAudio.createFromURL("",audio_dataList.get(i).getFile_path()));
                    }
                    player.initPlaylist(jcAudios, this);
                    if (!player.isPlaying()) {
                        iv_play.setVisibility(View.GONE);
                        progressBar.setVisibility(View.VISIBLE);
                        player.playAudio(player.getMyPlaylist().get(position));
                    }

                    //call api for check is favrite
                    checkIsFavorite();
            }

        }
    }

    public void initComponent() {

        tv_title      = findViewById(R.id.title);
        tv_sub_title  = findViewById(R.id.text_sub_title);
        tv_start_time = findViewById(R.id.text_start_time);
        tv_end_time   = findViewById(R.id.text_end_time);

        iv_audio_img  =  findViewById(R.id.image_view);
        iv_play       =  findViewById(R.id.image_view_play);
        iv_pause      =  findViewById(R.id.image_view_pause);
        iv_previous   =  findViewById(R.id.image_view_previous);
        iv_next       =  findViewById(R.id.image_view_next);
        iv_fav        =  findViewById(R.id.image_view_favorite);
        iv_fav_select =  findViewById(R.id.image_view_favorite_select);
        iv_repeat     =  findViewById(R.id.image_view_repeat);
        iv_ad_to_list =  findViewById(R.id.image_view_plus);

        linear_back   = findViewById(R.id.linear_back);
        linear_search = findViewById(R.id.linear_ic_search);

        //Progress bar
        progressBar = findViewById(R.id.progress_bar);

        //Find the player views
        player  = findViewById(R.id.jcplayer);
        seekBar = findViewById(R.id.simpleSeekBar);
        seekBar.setProgress(0);
        seekBar.setMax(100);
        seekBar.setOnSeekBarChangeListener(this);


        //Set listener on views
        linear_back.setOnClickListener(this);
        linear_search.setOnClickListener(this);
        iv_play.setOnClickListener(this);
        iv_pause.setOnClickListener(this);
        iv_next.setOnClickListener(this);
        iv_previous.setOnClickListener(this);
        iv_repeat.setOnClickListener(this);
        iv_fav.setOnClickListener(this);
        iv_fav_select.setOnClickListener(this);
        iv_ad_to_list.setOnClickListener(this);
    }

    public void checkIsFavorite() {
        if (audio_dataList.size() > 0) {
            Log.e("isFav","select = "+audio_dataList.get(position).getIs_favorite());
            if (audio_dataList.get(position).getIs_favorite() == 1) {
                iv_fav_select.setVisibility(View.VISIBLE);
                iv_fav.setVisibility(View.INVISIBLE);
            }
        }

    }

    public void loadAudioImage(String path,String title) {

        tv_title.setText(title);

        Picasso.get().load(path).into(iv_audio_img, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {

            }
        });
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.linear_back:
                finish();
                break;
            case R.id.linear_ic_search:

                break;
            case R.id.image_view_play:
                if (!player.isPlaying()) {
                    iv_play.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    player.playAudio(player.getMyPlaylist().get(position));
                }
                break;
            case R.id.image_view_pause:
                if (player.isPlaying()) {
                    player.pause();
                    iv_pause.setVisibility(View.INVISIBLE);
                    iv_play.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.image_view_next:
                 iv_pause.setVisibility(View.GONE);
                 iv_play.setVisibility(View.GONE);
                 progressBar.setVisibility(View.VISIBLE);
                if (position != total_lenth - 1) {
                    position = position +1;
                }
                if (position < total_lenth) {
                    player.playAudio(player.getMyPlaylist().get(position));
                    loadAudioImage(audio_dataList.get(position).getFile_image(),audio_dataList.get(position).getTitle());
                }
                checkIsFavorite();
                break;
            case R.id.image_view_previous:
                 iv_pause.setVisibility(View.GONE);
                 iv_play.setVisibility(View.GONE);
                 progressBar.setVisibility(View.VISIBLE);
                 if (position !=0) {
                    position = position - 1;
                 }
                 if (position < total_lenth) {
                    player.playAudio(player.getMyPlaylist().get(position));
                     loadAudioImage(audio_dataList.get(position).getFile_image(),audio_dataList.get(position).getTitle());
                 }
                 checkIsFavorite();
                break;
            case R.id.image_view_repeat:

                if (player.isPlaying()) {
                    iv_play.setVisibility(View.GONE);
                    iv_pause.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    player.playAudio(player.getMyPlaylist().get(position));
                }

                break;
            case R.id.image_view_favorite:
                 media_id = audio_dataList.get(position).getId();
                 addToFavorite();
                break;
            case R.id.image_view_favorite_select:
                media_id = audio_dataList.get(position).getId();
                addToFavorite();
                break;
            case R.id.image_view_plus:
                 Intent intent = new Intent(activity,Dialog_Playlist_Activity.class);
                 intent.putExtra("media_id",audio_dataList.get(position).getId());
                 startActivity(intent);
                break;
            default:break;
        }
    }


    @Override
    public void onPreparedAudio(JcStatus jcStatus) {

    }

    @Override
    public void onCompletedAudio() {
        onComplete();

    }

    @Override
    public void onPaused(JcStatus jcStatus) {

    }

    @Override
    public void onContinueAudio(JcStatus jcStatus) {

    }

    @Override
    public void onPlaying(JcStatus jcStatus) {

    }

    @Override
    public void onTimeChanged(JcStatus jcStatus) {
              int curretnPos = (int) jcStatus.getCurrentPosition();
              seekBar.setProgress(curretnPos);
              seekBar.setMax((int) jcStatus.getDuration());
              setText(getCurrentTime(jcStatus.getCurrentPosition()),getCurrentTime(jcStatus.getDuration()));
    }

    @Override
    public void onStopped(JcStatus jcStatus) {

    }

    @Override
    public void onJcpError(Throwable throwable) {

    }

    @Override
    public void onPathError(JcAudio jcAudio) {

    }

    private void onComplete() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                iv_play.setVisibility(View.VISIBLE);
                iv_pause.setVisibility(View.INVISIBLE);
                tv_start_time.setText(tv_end_time.getText());

                if (position != total_lenth - 1) {
                    position = position +1;
                }

                if (position < total_lenth) {
                    iv_play.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    player.pause();
                    player.playAudio(player.getMyPlaylist().get(position));
                    loadAudioImage(audio_dataList.get(position).getFile_image(),audio_dataList.get(position).getTitle());
                }
            }
        });
    }

    private void setText(final String start_time, final String end_time){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_start_time.setText(start_time);
                tv_end_time.setText(end_time);
                iv_play.setVisibility(View.INVISIBLE);
                iv_pause.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                if (!player.isPlaying()) {
                    iv_play.setVisibility(View.VISIBLE);
                    iv_pause.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        player.createNotification();
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.createNotification();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.kill();
    }

    String getCurrentTime(long ms) {
        ms /= 1000;
        return (ms / 3600) + ":" + ((ms % 3600) / 60) + ":" + ((ms % 3600) % 60);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (b) {
            iv_pause.setVisibility(View.INVISIBLE);
            iv_play.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            player.onProgressChanged(seekBar,i,b);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    //Method for add to favorite
    public void addToFavorite(){
        Api.getClient().addToFavorite(header,""+media_id).enqueue(new retrofit2.Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        if (response.body().isIs_favorite()) {
                            iv_fav.setVisibility(View.GONE);
                            iv_fav_select.setVisibility(View.VISIBLE);
//                            audio_dataList.get(position).setIs_favorite(1);
                        } else {
                            iv_fav.setVisibility(View.VISIBLE);
                            iv_fav_select.setVisibility(View.INVISIBLE);
                        }
                        ConstantMember.setBooleanValueInSession(activity,"is_update",true);
                        ConstantMember.showMessage(activity, "" + response.body().getMessage());
                    } else {
                        ConstantMember.showMessage(activity, "" + response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                ConstantMember.showMessage(activity,"Something went wrong please try again");
            }
        });
    }
}
