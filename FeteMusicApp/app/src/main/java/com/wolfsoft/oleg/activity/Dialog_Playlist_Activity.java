package com.wolfsoft.oleg.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.adapters.Dialog_List_Adapter;
import com.wolfsoft.oleg.bean.Featured_Video_Data;
import com.wolfsoft.oleg.bean.PlayListData;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;
import com.wolfsoft.oleg.utils.NetworkConnection;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dialog_Playlist_Activity extends AppCompatActivity {

    Context context;
    List<PlayListData> dataList = new ArrayList<>();

    String header,playlist_id,media_id,user_play_list,str_title;

    ImageView imageViewCreateList;
    TextView tv_cancel;
    RecyclerView recyclerView;
    Dialog_List_Adapter adapter;
    Gson gson = new Gson();
    Handler handler = new Handler();
    EditText editText_Playlist_Name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog__playlist_);
        context = Dialog_Playlist_Activity.this;
        header = ConstantMember.getHeader(context);

        initComponent();
        initListener();

        Intent intent = getIntent();
        media_id = ""+intent.getIntExtra("media_id",0);

        user_play_list = ConstantMember.getValueFromSession(context, "user_play_list");
        if (!user_play_list.equals("")) {
            dataList = gson.fromJson(user_play_list, new TypeToken<List<PlayListData>>() {}.getType());
            if (dataList.size() > 0) {
                callDialogAdapter();
            }
        }
    }

    public void initComponent(){
        imageViewCreateList = findViewById(R.id.image_view_add);
        tv_cancel = findViewById(R.id.btn_cancel);

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
    }

    public void initListener(){
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imageViewCreateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(context);
            }
        });
    }

    public void callDialogAdapter(){
        adapter = new Dialog_List_Adapter(context,dataList);
        recyclerView.setAdapter(adapter);
    }

    public void addToPlayList(String playlist_id) {
        DialogsUtils.showProgressDialog(context,"Please wait...");
        Api.getClient().addToPlayList(header,playlist_id,media_id).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, final Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ConstantMember.showMessage(context, "" + response.body().getMessage());
                                DialogsUtils.dismissDialog();
                                finish();
                            }
                        }, 1000);
                    } else {
                        ConstantMember.showMessage(context,""+response.body().getMessage());
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogsUtils.dismissDialog();
                            }
                        }, 1000);
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                DialogsUtils.dismissDialog();
                Log.e("dialog"," error= "+t);
            }
        });
    }


    public void showDialog(Context activity){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_box_create_playlist);

        editText_Playlist_Name = dialog.findViewById(R.id.edit_box_playlist_name);
        Button dialogButton,button_Cancel;

        dialogButton = dialog.findViewById(R.id.btn_submit);
        button_Cancel = dialog.findViewById(R.id.btn_cancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_title = editText_Playlist_Name.getText().toString();
                if (str_title.length() > 0) {
                    if (NetworkConnection.checkConnection(context)) {
                        apiCreatePlaylist();
                        dialog.dismiss();
                    } else {
                        ConstantMember.showMessage(context,"No internet connection");
                    }
                } else {
                    editText_Playlist_Name.setError("Please enter name");
                }
            }
        });

        button_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void apiCreatePlaylist() {
        DialogsUtils.showProgressDialog(context,"Please wait");
        Api.getClient().createUserPlayList(header,str_title).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        ConstantMember.showMessage(context,""+response.body().getMessage());
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogsUtils.dismissDialog();
                            }
                        },1000);

                    } else {
                        DialogsUtils.dismissDialog();
                        ConstantMember.showMessage(context,""+response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                DialogsUtils.dismissDialog();
                Log.e("error",""+t);
            }
        });
    }
}
