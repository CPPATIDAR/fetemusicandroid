package com.wolfsoft.oleg.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.bean.ForgotData;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.DialogsUtils;
import com.wolfsoft.oleg.utils.NetworkConnection;
import com.wolfsoft.oleg.utils.ConstantMember;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Forgot_Password_Activity extends AppCompatActivity {
    Context context;
    TextView tv_sign_in;

    String str_email;
    EditText editText_Email;
    FrameLayout frameLayout_Submit;

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot__password);
        context = Forgot_Password_Activity.this;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        initComponent();
        initListener();
    }

    public void initComponent(){
        tv_sign_in = findViewById(R.id.text_sign_in);
        editText_Email = findViewById(R.id.edit_box_email);
        frameLayout_Submit = findViewById(R.id.frame_layout_submit);
    }

    public void initListener(){

        tv_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        frameLayout_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.checkConnection(context)) {
                    if (validate()) {
                       apiCallForgotPassword();
                    }
                } else {
                    ConstantMember.showMessage(context,"No internet connection");
                }
            }
        });
    }

    public boolean validate() {
        boolean flag = true;

        str_email = ""+editText_Email.getText();

        if (str_email.length() == 0) {
            editText_Email.setError("Please enter email");
            flag = false;
        }

        if (ConstantMember.isValidEmail(str_email)) {
            editText_Email.setError("Please enter valid email");
            flag = false;
        }

        return  flag;
    }

    public void apiCallForgotPassword() {
        DialogsUtils.showProgressDialog(context,"Please wait...");
        Api.getClient().forgotPassword(str_email).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, final Response<SignUpResponse> response) {
                if(response.isSuccessful()){
                    assert response.body() != null;
                    if (response.body().isStatus()) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogsUtils.dismissDialog();
                                ConstantMember.showMessage(context, "" + response.body().getMessage());
                                ForgotData data = response.body().getForgotData();
                                Log.e("forgot ", "response= " + data.getUser_id() + "\n" + data.getForget_token());
                                preferences.edit().putString("user_id", "" + data.getUser_id()).apply();
                                preferences.edit().putString("forgot_token", "" + data.getForget_token()).apply();
                                startActivity(new Intent(context,Forgot_Update_Pssword_Activity.class));
                                finish();
                            }
                        }, 2000);
                    } else {
                        ConstantMember.showMessage(context, "" + response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
               DialogsUtils.dismissDialog();
               ConstantMember.showMessage(context,"Something went wrong");
            }
        });
    }

    //Api call for forgot password

}
