package com.wolfsoft.oleg.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;
import com.wolfsoft.oleg.utils.NetworkConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Forgot_Update_Pssword_Activity extends AppCompatActivity {

    Context context;
    TextView textView_SignIn;

    String str_user_id,str_forgot_token,str_otp,str_password,str_confirm_pass;
    EditText editText_Otp,editText_Password,editText_Confirm_Password;
    FrameLayout frameLayout_Submit;

    Handler handler = new Handler();
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot__update__pssword);

        context = Forgot_Update_Pssword_Activity.this;

        preferences      = PreferenceManager.getDefaultSharedPreferences(context);
        str_user_id      = preferences.getString("user_id","");
        str_forgot_token = preferences.getString("forgot_token","");

        Log.e("forgot detail","update activity = "+str_user_id+"\n"+str_forgot_token);

        initComponent();
        initListener();
    }

    public void initComponent() {
        editText_Otp              = findViewById(R.id.edit_box_otp);
        editText_Otp.setText(str_forgot_token);
        editText_Password         = findViewById(R.id.edit_box_new_password);
        editText_Confirm_Password = findViewById(R.id.edit_box_confirm_password);
        frameLayout_Submit        = findViewById(R.id.frame_layout_submit);
    }

    public void initListener() {
        frameLayout_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.checkConnection(context)) {
                    if (validate()) {
                        callUpdatePassword();
                    }
                }else {
                    ConstantMember.showMessage(context,"No internet connection");
                }
            }
        });
    }

    public boolean validate() {
        boolean flag = true;

        str_otp          = ""+editText_Otp.getText();
        str_password     = ""+editText_Password.getText();
        str_confirm_pass = ""+editText_Confirm_Password.getText();

        if(str_otp.length() == 0){
            editText_Otp.setError("Please enter the code");
            flag = false;
        }

        if(str_password.length() == 0){
            editText_Password.setError("Please enter password");
            flag = false;
        }

        if(str_confirm_pass.length() == 0){
            editText_Confirm_Password.setError("Please enter password");
            flag = false;
        }

        if (!str_password.equals(str_confirm_pass)) {
            editText_Confirm_Password.setError("Password mismatch");
            flag = false;
        }

        return flag;
    }

    public void callUpdatePassword() {

        DialogsUtils.showProgressDialog(context,"Please wait");
        Api.getClient().forgotUpdatePassword(str_user_id,str_forgot_token,str_password,str_confirm_pass).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, final Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogsUtils.dismissDialog();
                                ConstantMember.showMessage(context, "" + response.body().getMessage());
                                finish();
                            }
                        }, 2000);
                    } else {
                        ConstantMember.showMessage(context,""+response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                ConstantMember.showMessage(context,"Something went wrong");
            }
        });

    }
}
