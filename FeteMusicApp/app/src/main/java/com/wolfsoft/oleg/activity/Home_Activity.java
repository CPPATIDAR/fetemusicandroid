package com.wolfsoft.oleg.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.wolfsoft.oleg.fragments.Favorite_Fragment;
import com.wolfsoft.oleg.fragments.Featured_Artist_Fragment;
import com.wolfsoft.oleg.fragments.Fragment_Live;
import com.wolfsoft.oleg.fragments.Fragment_Live_Audio;
import com.wolfsoft.oleg.fragments.Fragment_Live_Video;
import com.wolfsoft.oleg.fragments.Fragment_PlayList_Create;
import com.wolfsoft.oleg.fragments.PlayList_Detail_Fragment;
import com.wolfsoft.oleg.R;

public class Home_Activity extends AppCompatActivity implements View.OnClickListener{

    LinearLayout linear_featured,linear_favorites,linear_playlists,linear_more,linear_live;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initComponent();
        initListener();

        //Loadt default fragment
        loadFragment(new Featured_Artist_Fragment());

    }

    public void initComponent(){

        linear_featured   = findViewById(R.id.linear_featured);
        linear_favorites  = findViewById(R.id.linear_favorites);
        linear_playlists  = findViewById(R.id.linear_playlists);
        linear_more       = findViewById(R.id.linear_more);
        linear_live       = findViewById(R.id.linear_live);

        linear_featured.setOnClickListener(this);
        linear_favorites.setOnClickListener(this);
        linear_playlists.setOnClickListener(this);
        linear_more.setOnClickListener(this);
        linear_live.setOnClickListener(this);

    }

    public void initListener(){

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Fragment fragment = null;
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        switch (id) {
            case R.id.linear_featured:
                if (!(currentFragment instanceof Featured_Artist_Fragment)) {
                    fragment = new Featured_Artist_Fragment();
                    loadFragment(fragment);
                }
                break;

            case R.id.linear_favorites:
                if (!(currentFragment instanceof Favorite_Fragment)) {
                    fragment = new Favorite_Fragment();
                    loadFragment(fragment);
                }
                break;
            case R.id.linear_playlists:
                if (!(currentFragment instanceof Fragment_PlayList_Create)) {
                    fragment = new Fragment_PlayList_Create();
                    loadFragment(fragment);
                }
                break;

            case R.id.linear_live:
                if (!(currentFragment instanceof Fragment_Live_Video)) {
                    fragment = new Fragment_Live_Video();
                    loadFragment(fragment);
                }
                break;
            case R.id.linear_more:
                  startActivity(new Intent(getApplicationContext(),Profile_Activity.class));
                break;
                default:break;
        }
    }

    private void loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
        }
    }
}
