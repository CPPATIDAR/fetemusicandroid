package com.wolfsoft.oleg.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bambuser.broadcaster.BroadcastPlayer;
import com.bambuser.broadcaster.LatencyMeasurement;
import com.bambuser.broadcaster.PlayerState;
import com.bambuser.broadcaster.SurfaceViewWithAutoAR;
import com.wolfsoft.oleg.R;

import okhttp3.OkHttpClient;

public class LivePlayerActivity extends AppCompatActivity {

    private static final String APPLICATION_ID = "e0t7lsBzazwjkxub1eWTrQ";
    private static final String API_KEY = "1q1slgn3yjb2jk6eszmrsmpah";

    String resourceUrl = "";

    private final Handler mMainHandler = new Handler();
    private final OkHttpClient mOkHttpClient = new OkHttpClient();
    private BroadcastPlayer mBroadcastPlayer = null;
    private SurfaceViewWithAutoAR mVideoSurfaceView = null;
    private SeekBar mVolumeSeekBar = null;
    private TextView mPlayerStatusTextView = null;
    private TextView mViewerStatusTextView = null;
    private TextView mBroadcastLiveTextView = null;
    private TextView mBroadcastLatencyTextView = null;
    private MediaController mMediaController = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_player);

        mPlayerStatusTextView = findViewById(R.id.PlayerStatusTextView);
        mBroadcastLiveTextView = findViewById(R.id.BroadcastLiveTextView);
        mBroadcastLatencyTextView = findViewById(R.id.BroadcastLatencyTextView);
        mVideoSurfaceView = findViewById(R.id.VideoSurfaceView);
        mVolumeSeekBar = findViewById(R.id.PlayerVolumeSeekBar);
        mVolumeSeekBar.setOnSeekBarChangeListener(mVolumeSeekBarListener);
        mViewerStatusTextView = findViewById(R.id.ViewerStatusTextView);
    }


    @Override
    protected void onResume () {
        super.onResume();
        mVideoSurfaceView = findViewById(R.id.VideoSurfaceView);
        mPlayerStatusTextView.setText("Loading latest broadcast");
        if (getIntent().hasExtra("resourceUri")) {
            resourceUrl = getIntent().getStringExtra("resourceUri");
            if (!resourceUrl.equals("")) {
                initPlayer(resourceUrl);
            }
        }
//			getLatestResourceUri();
    }

    @Override
    protected void onPause () {
        super.onPause();
        mOkHttpClient.dispatcher().cancelAll();
        setLatencyTimer(false);
        if (mBroadcastPlayer != null)
            mBroadcastPlayer.close();
        mBroadcastPlayer = null;
        mVideoSurfaceView = null;
        if (mMediaController != null)
            mMediaController.hide();
        mMediaController = null;
        if (mBroadcastLiveTextView != null)
            mBroadcastLiveTextView.setVisibility(View.GONE);
    }

    @Override
    public boolean onTouchEvent (MotionEvent ev){
        if (ev.getActionMasked() == MotionEvent.ACTION_UP && mBroadcastPlayer != null && mMediaController != null) {
            PlayerState state = mBroadcastPlayer.getState();
            if (state == PlayerState.PLAYING ||
                    state == PlayerState.BUFFERING ||
                    state == PlayerState.PAUSED ||
                    state == PlayerState.COMPLETED) {
                if (mMediaController.isShowing())
                    mMediaController.hide();
                else
                    mMediaController.show();
            } else {
                mMediaController.hide();
            }
        }
        return false;
    }

//    private void getLatestResourceUri () {
//        Request request = new Request.Builder()
//                .url("https://api.bambuser.com/broadcasts")
//                .addHeader("Accept", "application/vnd.bambuser.v1+json")
//                .addHeader("Content-Type", "application/json")
//                .addHeader("Authorization", "Bearer " + API_KEY)
//                .get()
//                .build();
//        mOkHttpClient.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(final Call call, final IOException e) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (mPlayerStatusTextView != null)
//                            mPlayerStatusTextView.setText("Http exception: " + e);
//                    }
//                });
//            }
//
//            @Override
//            public void onResponse(final Call call, final Response response) throws IOException {
//
////					Bundle extras = getIntent().getExtras();
////					if (extras != null) {
////
////						String resourceuri=extras.getString("resourceUri");
////						int pos= (int) extras.get("position");
////
////						Log.e("recieve url   ","getting through intent  = "+resourceuri);
////						Log.e("recieve position ","getting through intent = "+pos);
////
////
////					runOnUiThread(new Runnable() {
////						@Override
////						public void run() {
////							initPlayer(resourceuri);
////						}
////					});}
//            }
//        });
//    }

    private void initPlayer (String resourceUri){
        if (resourceUri == null) {
            if (mPlayerStatusTextView != null)
                mPlayerStatusTextView.setText("Could not get info about latest broadcast");
            return;
        }
        if (mVideoSurfaceView == null) {
            // UI no longer active
            return;
        }
        if (mBroadcastPlayer != null)
            mBroadcastPlayer.close();
        mBroadcastPlayer = new BroadcastPlayer(this, resourceUri, APPLICATION_ID, mPlayerObserver);
        mBroadcastPlayer.setSurfaceView(mVideoSurfaceView);
        mBroadcastPlayer.setAcceptType(BroadcastPlayer.AcceptType.ANY);
        mBroadcastPlayer.setViewerCountObserver(mViewerCountObserver);
        updateVolume(mVolumeSeekBar.getProgress() / (float) mVolumeSeekBar.getMax());
        mBroadcastPlayer.load();
    }

    private void updateVolume ( float progress){
        // Output volume should optimally increase logarithmically, but Android media player APIs
        // respond linearly. Producing non-linear scaling between 0.0 and 1.0 by using x^4.
        // Not exactly logarithmic, but has the benefit of satisfying the end points exactly.
        if (mBroadcastPlayer != null)
            mBroadcastPlayer.setAudioVolume(progress * progress * progress * progress);
    }


    private final SeekBar.OnSeekBarChangeListener mVolumeSeekBarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            updateVolume(seekBar.getProgress() / (float) seekBar.getMax());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };


    private final BroadcastPlayer.Observer mPlayerObserver = new BroadcastPlayer.Observer() {
        @Override
        public void onStateChange(PlayerState state) {
            if (mPlayerStatusTextView != null)
                mPlayerStatusTextView.setText("Status: " + state);
            boolean isPlayingLive = mBroadcastPlayer != null && mBroadcastPlayer.isTypeLive() && mBroadcastPlayer.isPlaying();
            if (mBroadcastLiveTextView != null) {
                mBroadcastLiveTextView.setVisibility(isPlayingLive ? View.VISIBLE : View.GONE);
            }
            updateLatencyView();
            setLatencyTimer(isPlayingLive);
            if (state == PlayerState.PLAYING || state == PlayerState.PAUSED || state == PlayerState.COMPLETED) {
                if (mMediaController == null && mBroadcastPlayer != null && !mBroadcastPlayer.isTypeLive()) {
                    mMediaController = new MediaController(LivePlayerActivity.this);
                    mMediaController.setAnchorView(mVideoSurfaceView);
                    mMediaController.setMediaPlayer(mBroadcastPlayer);
                }
                if (mMediaController != null) {
                    mMediaController.setEnabled(true);
                    mMediaController.show();
                }
            } else if (state == PlayerState.ERROR || state == PlayerState.CLOSED) {
                if (mMediaController != null) {
                    mMediaController.setEnabled(false);
                    mMediaController.hide();
                }
                mMediaController = null;
                if (mViewerStatusTextView != null)
                    mViewerStatusTextView.setText("");
            }
        }

        @Override
        public void onBroadcastLoaded(boolean live, int width, int height) {
            if (mBroadcastLiveTextView != null)
                mBroadcastLiveTextView.setVisibility(live ? View.VISIBLE : View.GONE);
        }
    };



    private void updateLatencyView () {
        if (mBroadcastLatencyTextView != null) {
            LatencyMeasurement lm = mBroadcastPlayer != null ? mBroadcastPlayer.getEndToEndLatency() : null;
            if (lm != null)
                mBroadcastLatencyTextView.setText("Latency: " + (lm.latency / 1000.0) + " s");
                mBroadcastLatencyTextView.setVisibility(View.GONE);
//               mBroadcastLatencyTextView.setVisibility(lm != null ? View.VISIBLE : View.GONE);
        }
    }

    private void setLatencyTimer ( boolean enable){
        mMainHandler.removeCallbacks(mLatencyUpdateRunnable);
        if (enable)
            mMainHandler.postDelayed(mLatencyUpdateRunnable, 1000);
    }

    private final Runnable mLatencyUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            updateLatencyView();
            mMainHandler.postDelayed(this, 1000);
        }
    };
    private final BroadcastPlayer.ViewerCountObserver mViewerCountObserver = new BroadcastPlayer.ViewerCountObserver() {
        @Override
        public void onCurrentViewersUpdated(long viewers) {
            if (mViewerStatusTextView != null)
                mViewerStatusTextView.setText("Viewers: " + viewers);
        }

        @Override
        public void onTotalViewersUpdated(long viewers) {
        }
    };


}
