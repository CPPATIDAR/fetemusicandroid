package com.wolfsoft.oleg.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.adapters.Play_List_Detail_Adapter;
import com.wolfsoft.oleg.bean.Featured_Audio_Data;
import com.wolfsoft.oleg.bean.PlayListData;
import com.wolfsoft.oleg.bean.PlayListMediaData;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;
import com.wolfsoft.oleg.utils.NetworkConnection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayList_Detail_Activity extends AppCompatActivity {

    Context context;
    RecyclerView recyclerViewPlayList;
    List<Featured_Audio_Data> mediaDataList;
    Play_List_Detail_Adapter adapter;

    String playlist_id,type = "audio";
    int page_no = 1;
    String header;
    ImageView imageView_Back;
    LinearLayout playBtn;
    ImageView iv_fav,iv_fav_select,addToPlaylist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_list__detail_);

        context = PlayList_Detail_Activity.this;
        header  = ConstantMember.getHeader(context);

        initComponent();
        initListener();

        Intent intent = getIntent();
        playlist_id = ""+intent.getStringExtra("playlist_id");
        Log.e("playlist "," id= "+playlist_id);

        if (NetworkConnection.checkConnection(context)) {
            getPlaylistSong();
        } else {
            ConstantMember.showMessage(context,"No internet connection");
        }
    }

    public void initComponent() {

        imageView_Back = findViewById(R.id.image_view_back);

        recyclerViewPlayList = findViewById(R.id.recycler);
        recyclerViewPlayList.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        playBtn=findViewById(R.id.play);

        iv_fav=findViewById(R.id.image_view_fav);
        iv_fav_select=findViewById(R.id.image_view_fav_select);


        addToPlaylist=findViewById(R.id.addto_playlist);

    }

    public void initListener() {
        imageView_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(mediaDataList.size()>0) {

                    Gson gson = new Gson();
                    String str_list = gson.toJson(mediaDataList);
                    ConstantMember.setValueInSession(context, "user_playlist", str_list);
                    Intent intent = new Intent(context, Audio_Player_Activity.class);
                    intent.putExtra("position", 0);
                    intent.putExtra("comming_from", "");
                    context.startActivity(intent);
                }

                else{

                    getPlaylistSong();
                }

            }
        });


        iv_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addToFavorite();
            }
        });

        addToPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,Dialog_Playlist_Activity.class);
                intent.putExtra("media_id",0);
                startActivity(intent);
            }
        });
    }

    private void addToFavorite() {

        Api.getClient().addToFavorite(header,"0").enqueue(new retrofit2.Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {

                        if (response.body().isIs_favorite()) {

                            iv_fav.setVisibility(View.GONE);
                            iv_fav_select.setVisibility(View.VISIBLE);
                            mediaDataList.get(0).setIs_favorite(1);

                        }
                    }
                    ConstantMember.showMessage(context, "" + response.body().getMessage());
                } else {
                    ConstantMember.showMessage(context, "" + response.body().getMessage());
                }
            }


            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                ConstantMember.showMessage(context,"Something went wrong please try again");
            }
        });
    }

    public void getPlaylistSong() {
        DialogsUtils.showProgressDialog(context,"Please wait...");
        Api.getClient().getPlayListMedia(header,playlist_id,type,""+page_no).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        mediaDataList = response.body().getMediaList();
                        callAdapter();
                        DialogsUtils.dismissDialog();

                        if (mediaDataList.get(0).getIs_favorite()==1) {
                            iv_fav.setVisibility(View.GONE);
                            iv_fav_select.setVisibility(View.VISIBLE);
                        }

                    }
                } else {
                    DialogsUtils.dismissDialog();
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                DialogsUtils.dismissDialog();
            }
        });
    }

    public void callAdapter() {
        if (mediaDataList.size() > 0) {
            adapter = new Play_List_Detail_Adapter(context,mediaDataList);
            recyclerViewPlayList.setAdapter(adapter);
        }
    }
}
