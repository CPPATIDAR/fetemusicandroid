package com.wolfsoft.oleg.activity;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.bean.CountryData;
import com.wolfsoft.oleg.bean.CountryList;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.bean.UserLoginData;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;
import com.wolfsoft.oleg.utils.NetworkConnection;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Profile_Activity extends AppCompatActivity {

    private static final int REQUEST_CAPTURE_IMAGE = 100,REQUEST_CAPTURE_IMAGE_GALLERY = 200;

    Context context;
    ImageView imageView_Submit,imageViewProfile;
    TextView editText_Country,editText_Status;
    String  str_email,str_name,str_dob,str_genre,str_status,str_country_id,
            str_password,str_country_name,token_type,access_token,headers,str_user_pic_url;
    String imageFilePath;
    EditText editText_Email,editText_Name,editText_DoB,editText_Genre,editText_Password;

    LinearLayout linearLayout_Country,linearLayout_Status;
    SharedPreferences preferences;
    Handler handler = new Handler();

    List<CountryData> list = new ArrayList<>();
    MultipartBody.Part multi_part_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        context = Profile_Activity.this;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);

        token_type   = preferences.getString("token_type","");
        access_token = preferences.getString("accessToken","");
        headers = token_type+" "+access_token;

        initComponent();
        initListener();

        apiGetCountry();
    }

    public void initComponent() {
        editText_Email    = findViewById(R.id.edit_box_email);
        editText_Name     = findViewById(R.id.edit_box_user_name);
        editText_DoB      = findViewById(R.id.edit_box_date);
        editText_Genre    = findViewById(R.id.edit_box_genre);
        editText_Status   = findViewById(R.id.edit_box_status);
        editText_Country  = findViewById(R.id.edit_box_country);
        editText_Password = findViewById(R.id.edit_box_password);
        imageView_Submit  = findViewById(R.id.image_view_submit);
        imageViewProfile  = findViewById(R.id.profile_image);

        linearLayout_Country = findViewById(R.id.linear_country_box);
        linearLayout_Status  = findViewById(R.id.linear_status_box);

        //Set user detail from session
        str_email  = preferences.getString("email","");
        str_name   = preferences.getString("name","");
        str_dob    = preferences.getString("age","");
        str_status = preferences.getString("relationship_status","");
        str_genre  = preferences.getString("genre","");
        str_country_name = preferences.getString("country_name","");
        str_country_id  = preferences.getString("country_id","");
        str_user_pic_url = preferences.getString("profile_pic","");

        //Set the user profile pic
        setUserProfile(str_user_pic_url);

        editText_Email.setText(str_email);
        editText_Name.setText(str_name);
        editText_DoB.setText(str_dob);
        editText_Status.setText(str_status);
        editText_Genre.setText(str_genre);
        editText_Country.setText(str_country_name);
    }

    public void initListener() {

        linearLayout_Status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(context, R.style.PopupMenuTextView);
                PopupMenu menu = new PopupMenu(wrapper,v);
                menu.getMenu().add(1, 1, 1, "Single");
                menu.getMenu().add(1, 2, 2, "Married");

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        editText_Status.setText(item.getTitle());
                        return false;
                    }
                });
                menu.show();
            }
        });

        linearLayout_Country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCountryList(view);
            }
        });

        imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConstantMember.checkPermission(Profile_Activity.this)) {
//                    openCameraIntent();
                    selectImage();
                }
            }
        });

        imageView_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.checkConnection(context)) {
                    if (validate()) {
                        updateProfile();
                    }
                } else {
                    ConstantMember.showMessage(context,"No internet connection");
                }
            }
        });
    }

    public boolean validate(){
        boolean flag = true;

        str_email      = ""+editText_Email.getText();
        str_name       = ""+editText_Name.getText();
        str_dob        = ""+editText_DoB.getText();
        str_genre      = ""+editText_Genre.getText();
        str_status     = ""+editText_Status.getText();
        str_country_name = ""+editText_Country.getText();
        str_password   = ""+editText_Password.getText();

//        if (str_email.length() == 0) {
//            editText_Email.setError("Please enter the email");
//            flag = false;
//        }
//
//        if (!ConstantMember.isValidEmail(str_email)) {
//            editText_Email.setError("Please enter valid email");
//            flag = false;
//        }

        if (str_name.length() == 0) {
            editText_Name.setError("Please enter the name");
            flag = false;
        }

        if (str_dob.length() == 0) {
            editText_DoB.setError("Please enter the age");
            flag = false;
        }

        if (str_genre.length() == 0) {
            editText_Genre.setError("Please enter the genre");
            flag = false;
        }

        if (str_status.length() == 0) {
            editText_Status.setError("Please select the status");
            flag = false;
        }

        if (str_country_name.length() == 0) {
            editText_Country.setError("Please select the country");
            flag = false;
        }

//        if (str_password.length() == 0) {
//            editText_Password.setError("Please enter the password");
//            flag = false;
//        }

        return  flag;
    }

    public void setUserProfile(String url) {
        if (!url.equals("")) {
            Picasso.get().load(url).into(imageViewProfile, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {

                }
            });
        }
    }

    public void updateProfile() {
        str_country_id = ""+2;
        Log.e("update","profile params= "+str_name+"\n"+"age = "+str_dob+"\n"+
                   "relation status= "+str_status+ "\n"+
                   "genre = "+str_genre+"\n"+
                   "country id = "+str_country_id+"\n\n"+
                   "headers = "+headers);

        DialogsUtils.showProgressDialog(context,"Please wait...");
        RequestBody email_body = RequestBody.create(MediaType.parse("text/plain"), ""+editText_Email.getText());
        RequestBody str_name_body = RequestBody.create(MediaType.parse("text/plain"), str_name);
        RequestBody str_dob_body = RequestBody.create(MediaType.parse("text/plain"), str_dob);
        RequestBody str_status_body = RequestBody.create(MediaType.parse("text/plain"), str_status);
        RequestBody str_genre_body = RequestBody.create(MediaType.parse("text/plain"), str_genre);
        RequestBody str_country_id_body = RequestBody.create(MediaType.parse("text/plain"), str_country_id);

        Api.getClient().updateProfile(headers,
                str_name_body,
                str_dob_body,
                str_status_body,
                str_genre_body,
                str_country_id_body,
                email_body,
                multi_part_image).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, final Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogsUtils.dismissDialog();
                                ConstantMember.showMessage(context,""+response.body().getMessage());
                                Log.e("image path","url = "+response.body().getUserData().getProfile_picture());
                                updateData(response.body().getUserData());
                            }
                        },2000);

                    }else {
                        ConstantMember.showMessage(context,""+response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                DialogsUtils.dismissDialog();
                Log.e("error","login= "+t);
                ConstantMember.showMessage(context,"Something went wrong try again");
            }
        });
    }

    public void updateData(UserLoginData userData) {

        preferences.edit().putString("email",""+userData.getEmail()).apply();
        editText_Email.setText(""+userData.getEmail());

        preferences.edit().putString("name",""+userData.getName()).apply();
        editText_Name.setText(""+userData.getName());

        preferences.edit().putString("age", ""+userData.getAge()).apply();
        editText_DoB.setText(""+userData.getAge());

        preferences.edit().putString("relationship_status",""+userData.getRelationship_status()).apply();
        editText_Status.setText(""+userData.getRelationship_status());

        preferences.edit().putString("genre",""+userData.getFavorite_soca_genre()).apply();
        editText_Genre.setText(""+userData.getFavorite_soca_genre());

        preferences.edit().putString("country_name",""+str_country_name).apply();
        editText_Country.setText(""+str_country_name);

        preferences.edit().putString("country_id",""+str_country_id).apply();

        preferences.edit().putString("profile_pic",userData.getProfile_picture()).apply();
    }


    public void apiGetCountry() {
        Api.getClient().getCountryList().enqueue(new Callback<CountryList>() {
            @Override
            public void onResponse(Call<CountryList> call, Response<CountryList> response) {
                if (response.isSuccessful()) {
                    CountryList data = response.body();
                    list = new ArrayList<>(data.getCountrieList());
                }
            }

            @Override
            public void onFailure(Call<CountryList> call, Throwable t) {
                Log.e("listdata","res err = "+t);
            }
        });
    }

    public void showCountryList(View v){

        Context wrapper = new ContextThemeWrapper(context, R.style.PopupMenuTextView);
        PopupMenu menu = new PopupMenu(wrapper,v);
        if(list.size()>0){
            for (int i = 0; i<list.size(); i++){
                menu.getMenu().add(1, list.get(i).getId(), list.get(i).getId(), ""+list.get(i).getTitle());
            }
        }
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                str_country_id = ""+item.getItemId();
                str_country_name = ""+item.getTitle();
                editText_Country.setText(str_country_name);
                return false;
            }
        });
        menu.show();

    }

    //Code for get image from camera and gallery
    private void selectImage() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")){
                    Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if(pictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(pictureIntent,
                                REQUEST_CAPTURE_IMAGE);
                    }
                } else if (options[item].equals("Choose from Gallery")){
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, REQUEST_CAPTURE_IMAGE_GALLERY);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,Intent data) {
        if (requestCode == REQUEST_CAPTURE_IMAGE &&
                resultCode == RESULT_OK) {
            if (data != null && data.getExtras() != null) {
                Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                imageViewProfile.setImageBitmap(imageBitmap);
                String image = saveImage(imageBitmap);
                multi_part_image = uploadToServer(image);
            }
        }

        if (requestCode == REQUEST_CAPTURE_IMAGE_GALLERY &&
                resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                Uri uri = data.getData();
                Bitmap imageBitmap = null;
                try {
                    imageBitmap = getBitmapFromUri(uri);
                    imageViewProfile.setImageBitmap(imageBitmap);
                    String image = saveImage(imageBitmap);
                    multi_part_image = uploadToServer(image);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    private MultipartBody.Part  uploadToServer(String filePath) {
        //Create a file object using file path
        File file = new File(filePath);
        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("profile_picture", file.getName(), fileReqBody);
        //Create request body with text description and text media type
        return part;
    }

    public String saveImage(Bitmap bitmap){

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Fete_Music");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "FILENAME-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            Toast.makeText(context, "Image Saved", Toast.LENGTH_SHORT).show();
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return  file.getAbsolutePath();
    }
}
