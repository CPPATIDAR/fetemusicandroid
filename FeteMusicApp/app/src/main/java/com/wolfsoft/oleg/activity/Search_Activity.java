package com.wolfsoft.oleg.activity;

import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.adapters.SearchAdapterAudio;
import com.wolfsoft.oleg.adapters.SearchAdapterPlaylist;
import com.wolfsoft.oleg.adapters.SearchAdapterVideo;
import com.wolfsoft.oleg.adapters.SearchAdpaterArtist;
import com.wolfsoft.oleg.bean.ArtistData;
import com.wolfsoft.oleg.bean.FeaturedResponse;
import com.wolfsoft.oleg.bean.Featured_Audio_Data;
import com.wolfsoft.oleg.bean.Featured_Video_Data;
import com.wolfsoft.oleg.bean.PlayListData;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Search_Activity extends AppCompatActivity implements View.OnClickListener {

    Context context;
    ImageView iv_back;
    EditText editTextSearch;

    String header,strSearcText;
    int pageNo = 1;

    RecyclerView recyclerViewArtist,recyclerViewVideo,recyclerViewAudio,recyclerViewPlaylist;

    LinearLayout linear_artist,linear_video,linear_audio,linear_playlist;

    List<ArtistData> listArtistData = new ArrayList<>();
    List<Featured_Video_Data> listFeaturedVideo = new ArrayList<>();
    List<Featured_Audio_Data> listFeaturedSong = new ArrayList<>();
    List<PlayListData> listPlaylist = new ArrayList<>();

    SearchAdpaterArtist adapterArtist;
    SearchAdapterVideo  adapterVideo;
    SearchAdapterAudio  adapterAudio;
    SearchAdapterPlaylist adapterPlaylist;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (strSearcText.length() > 0) {
                apiCallSearch();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        context = Search_Activity.this;
        header  = ConstantMember.getHeader(context);
        initComponent();
        initListener();
    }

    public void initComponent() {
        iv_back = findViewById(R.id.img_back);
        iv_back.setOnClickListener(this);

        editTextSearch = findViewById(R.id.edit_search_text);

        recyclerViewArtist = findViewById(R.id.recycler_search_artist);
        recyclerViewArtist.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        recyclerViewVideo  = findViewById(R.id.recycler_search_video);
        recyclerViewVideo.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        recyclerViewAudio  = findViewById(R.id.recycler_search_audio);
        recyclerViewAudio.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        recyclerViewPlaylist = findViewById(R.id.recycler_search_playlist);
        recyclerViewPlaylist.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        //Find the linear views
        linear_artist = findViewById(R.id.linear_artist);
        linear_video = findViewById(R.id.linear_video);
        linear_audio = findViewById(R.id.linear_audio);
        linear_playlist = findViewById(R.id.linear_playlist);

    }

    public void initListener() {
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                strSearcText = editable.toString();
                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable,2000);
            }
        });
    }

    public void apiCallSearch() {
        DialogsUtils.showProgressDialog(context,"Please wait...");

        Log.e("search ","text = "+strSearcText.length());
        Api.getClient().getSearchResult(header,strSearcText,""+pageNo).enqueue(new Callback<FeaturedResponse>() {
            @Override
            public void onResponse(Call<FeaturedResponse> call, Response<FeaturedResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body().isStatus()) {
                        Log.e("search ","response = "+response.body().getFeaturedData().getArtistDataList());
                        listArtistData    = response.body().getFeaturedData().getArtistDataList();
                        listFeaturedSong  = response.body().getFeaturedData().getFeaturedSongList();
                        listFeaturedVideo = response.body().getFeaturedData().getFeaturedVideoList();
                        listPlaylist      = response.body().getFeaturedData().getPlayListData();

                        callAdapterArtist();
                        callAdapterVideo();
                        callAdapterAudio();
                        callAdapterPlaylist();

                        DialogsUtils.dismissDialog();

                    }

                    else{

                        DialogsUtils.showProgressDialog(context,"Please wait...");

                    }
                }
            }

            @Override
            public void onFailure(Call<FeaturedResponse> call, Throwable t) {
                Log.e("search ","error = "+t);
            }
        });
    }

    public void callAdapterArtist() {
        if (listArtistData.size() > 0) {
            linear_artist.setVisibility(View.VISIBLE);
            adapterArtist = new SearchAdpaterArtist(context,listArtistData);
            recyclerViewArtist.setAdapter(adapterArtist);
        }
    }

    public void callAdapterVideo() {
        if (listFeaturedVideo.size() > 0) {
            linear_video.setVisibility(View.VISIBLE);
            adapterVideo = new SearchAdapterVideo(context,listFeaturedVideo);
            recyclerViewVideo.setAdapter(adapterVideo);
        }
    }

    public void callAdapterAudio() {
        if (listFeaturedSong.size() > 0) {
            linear_audio.setVisibility(View.VISIBLE);
            adapterAudio = new SearchAdapterAudio(context,listFeaturedSong);
            recyclerViewAudio.setAdapter(adapterAudio);
        }
    }

    public void callAdapterPlaylist() {
        if (listPlaylist.size() > 0) {
            linear_playlist.setVisibility(View.VISIBLE);
            adapterPlaylist = new SearchAdapterPlaylist(context,listPlaylist);
            recyclerViewPlaylist.setAdapter(adapterPlaylist);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }
}
