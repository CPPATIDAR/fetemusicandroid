package com.wolfsoft.oleg.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.bean.UserLoginData;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.DialogsUtils;
import com.wolfsoft.oleg.utils.NetworkConnection;
import com.wolfsoft.oleg.utils.ConstantMember;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignIn_Activity extends AppCompatActivity {
    Context context;
    String str_email,str_password;

    TextView tv_create_account,tv_forgot_password;
    EditText editText_Email,editText_Password;
    FrameLayout layout_enter_button;

    Handler handler = new Handler();
    SharedPreferences preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        context = SignIn_Activity.this;
        preferenceManager = PreferenceManager.getDefaultSharedPreferences(context);
        initComponent();
        initListener();
    }

    public void initComponent() {
        tv_create_account   = findViewById(R.id.text_create_acount);
        tv_forgot_password  = findViewById(R.id.text_forgot_password);

        editText_Email      = findViewById(R.id.edit_box_email);
        editText_Password   = findViewById(R.id.edit_box_password);
        layout_enter_button = findViewById(R.id.frame_layout_enter);
    }

    public void  initListener() {

        tv_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context,SignUpActivity.class));
            }
        });

        tv_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context,Forgot_Password_Activity.class));
//                startActivity(new Intent(context,Home_Activity.class));
            }
        });

        layout_enter_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.checkConnection(getApplicationContext())) {
                    if (validate()) {
                       apiCallLogin();
                    }
                }else {
                    ConstantMember.showMessage(getApplicationContext(),"No internet connection");
                }
            }
        });
    }

    public boolean validate() {
        boolean flag = true;
        str_email    = ""+editText_Email.getText();
        str_password = ""+editText_Password.getText();

        if (str_email.length() == 0) {
            editText_Email.setError("Please enter the email");
            flag = false;
        }

        if (ConstantMember.isValidEmail(str_email)) {
            editText_Email.setError("Please enter valid email");
            flag = false;
        }

        if (str_password.length() == 0) {
            editText_Password.setError("Please enter the password");
            flag = false;
        }
        return flag;
    }

    public void apiCallLogin(){

        Log.e("listdata","params= "+str_email+"\n"+
                "Password= "+str_password+"\n");
        DialogsUtils.showProgressDialog(context,"Please wait...");
        Api.getClient().loginUser(str_email,str_password).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, final Response<SignUpResponse> response) {
                final SignUpResponse signUpResponse = response.body();
                Log.e("list","Register response = "+response.isSuccessful()+" status = "+signUpResponse.isStatus());
                if(signUpResponse.isStatus()){
                    ConstantMember.showMessage(context,""+signUpResponse.getMessage());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            DialogsUtils.dismissDialog();
                            enterInApp(signUpResponse);
                        }
                    },2000);
                }else {
                    DialogsUtils.dismissDialog();
                    ConstantMember.showMessage(context,""+signUpResponse.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                DialogsUtils.dismissDialog();
                Log.e("error","login= "+t);
                ConstantMember.showMessage(context,"Something went wrong try again");
            }
        });

    }

    public void enterInApp(SignUpResponse response) {
        UserLoginData userLoginData = response.getUserData();
        preferenceManager.edit().putString("accessToken",""+response.getAccess_token()).apply();
        preferenceManager.edit().putString("token_type",""+response.getToken_type()).apply();

        UserLoginData userData = response.getUserData();
        preferenceManager.edit().putString("email",""+userData.getEmail()).apply();
        preferenceManager.edit().putString("name",""+userData.getName()).apply();
        preferenceManager.edit().putString("age", ""+userData.getAge()).apply();
        preferenceManager.edit().putString("relationship_status",""+userData.getRelationship_status()).apply();
        preferenceManager.edit().putString("genre",""+userData.getFavorite_soca_genre()).apply();
        preferenceManager.edit().putString("country_name",""+userData.getCountry_name()).apply();
        preferenceManager.edit().putString("country_id",""+userData.getCountry_id()).apply();
        preferenceManager.edit().putString("profile_pic",userData.getProfile_picture()).apply();
        preferenceManager.edit().putBoolean("is_log_in",true).apply();

        startActivity(new Intent(context,Home_Activity.class));
        finish();
    }
}
