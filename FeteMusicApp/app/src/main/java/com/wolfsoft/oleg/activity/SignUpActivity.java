package com.wolfsoft.oleg.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.bean.CountryData;
import com.wolfsoft.oleg.bean.CountryList;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;
import com.wolfsoft.oleg.utils.NetworkConnection;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {
    Context context;
    TextView tv_sign_in,editText_Country,editText_Status;
    ImageView imageView_Submit;

    String  str_email,str_name,str_dob,str_genre,str_status,str_country_id,str_password,str_country_name;
    EditText editText_Email,editText_Name,editText_DoB,editText_Genre,editText_Password;

    LinearLayout linearLayout_Country,linearLayout_Status;
    Handler handler = new Handler();
    List<CountryData> list = new ArrayList<>();
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        context = SignUpActivity.this;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        initComponent();
        initListener();

        //Api call for list of country
        apiGetCountry();
    }

    public void initComponent() {
        tv_sign_in  = findViewById(R.id.text_sign_in);
        //Find the edit text fields
        editText_Email    = findViewById(R.id.edit_box_email);
        editText_Name     = findViewById(R.id.edit_box_user_name);
        editText_DoB      = findViewById(R.id.edit_box_date);
        editText_Genre    = findViewById(R.id.edit_box_genre);
        editText_Status   = findViewById(R.id.edit_box_status);
        editText_Country  = findViewById(R.id.edit_box_country);
        editText_Password = findViewById(R.id.edit_box_password);
        imageView_Submit  = findViewById(R.id.image_view_submit);

        linearLayout_Country = findViewById(R.id.linear_country_box);
        linearLayout_Status  = findViewById(R.id.linear_status_box);
    }

    public void  initListener() {
        tv_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imageView_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.checkConnection(getApplicationContext())) {
                    if (validate()) {
                       registerUser();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),"Please check internet connection",Toast.LENGTH_LONG).show();
                }
            }
        });

        linearLayout_Country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCountryList(view);
            }
        });

        linearLayout_Status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(context, R.style.PopupMenuTextView);
                PopupMenu menu = new PopupMenu(wrapper,v);
                        menu.getMenu().add(1, 1, 1, "Single");
                        menu.getMenu().add(1, 2, 2, "Married");

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        editText_Status.setText(item.getTitle());
                        return false;
                    }
                });
                menu.show();
            }
        });
    }

   public boolean validate(){
        boolean flag = true;

        str_email      = ""+editText_Email.getText();
        str_name       = ""+editText_Name.getText();
        str_dob        = ""+editText_DoB.getText();
        str_genre      = ""+editText_Genre.getText();
        str_status     = ""+editText_Status.getText();
        str_country_name = ""+editText_Country.getText();
        str_password   = ""+editText_Password.getText();

       if (str_email.length() == 0) {
           editText_Email.setError("Please enter the email");
           flag = false;
       }

       if (ConstantMember.isValidEmail(str_email)) {
           editText_Email.setError("Please enter valid email");
           flag = false;
       }

       if (str_name.length() == 0) {
           editText_Name.setError("Please enter the name");
           flag = false;
       }

       if (str_dob.length() == 0) {
           editText_DoB.setError("Please enter the age");
           flag = false;
       }

       if (str_genre.length() == 0) {
           editText_Genre.setError("Please enter the genre");
           flag = false;
       }

       if (str_status.length() == 0) {
           editText_Status.setError("Please select the status");
           flag = false;
       }

       if (str_country_id.length() == 0) {
           editText_Country.setError("Please select the country");
           flag = false;
       }

       if (str_password.length() == 0) {
           editText_Password.setError("Please enter the password");
           flag = false;
       }

        return  flag;
   }

   //Method for register user
    public void registerUser() {
        DialogsUtils.showProgressDialog(context,"Please wait...");
        Api.getClient().registerUser(str_email,str_name,str_password,str_dob,str_status,str_genre,str_country_id).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                SignUpResponse signUpResponse = response.body();
                Log.e("list","Register response = "+response.isSuccessful()+"status = "+response.body().isStatus());
                if(signUpResponse.isStatus()){
                    showMessage(""+signUpResponse.getMessage());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                          DialogsUtils.dismissDialog();
                          finish();
                        }
                    },2000);
                }else {
                    DialogsUtils.dismissDialog();
                    showMessage(""+signUpResponse.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                DialogsUtils.dismissDialog();
                showMessage("Something went wrong try again");
            }
        });
    }

    public void apiGetCountry() {
        Api.getClient().getCountryList().enqueue(new Callback<CountryList>() {
            @Override
            public void onResponse(Call<CountryList> call, Response<CountryList> response) {
                if (response.isSuccessful()) {
                    CountryList data = response.body();
                    list = new ArrayList<>(data.getCountrieList());
                    if (list.size()>0) {
                        preferences.edit().putString("country_name",""+list.get(0).getTitle()).apply();
                    }
                }
            }

            @Override
            public void onFailure(Call<CountryList> call, Throwable t) {
                Log.e("listdata","res err = "+t);
            }
        });
    }

    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }

    public void showCountryList(View v){

        Context wrapper = new ContextThemeWrapper(context, R.style.PopupMenuTextView);
        PopupMenu menu = new PopupMenu(wrapper,v);
        if(list.size()>0){
            for (int i = 0; i<list.size(); i++){
                menu.getMenu().add(1, list.get(i).getId(), list.get(i).getId(), ""+list.get(i).getTitle());
            }
        }
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                str_country_id = ""+item.getItemId();
                str_country_name = ""+item.getTitle();
                editText_Country.setText(str_country_name);
                preferences.edit().putString("country_name",""+str_country_name).apply();
                return false;
            }
        });
        menu.show();

    }

}
