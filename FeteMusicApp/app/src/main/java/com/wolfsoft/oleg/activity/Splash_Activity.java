package com.wolfsoft.oleg.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.utils.ConstantMember;

public class Splash_Activity extends AppCompatActivity {

    Context context;
    Handler handler = new Handler();
    boolean isLogIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = Splash_Activity.this;
        isLogIn = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("is_log_in",false);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isLogIn) {
                    startActivity(new Intent(context, Home_Activity.class));
                } else {
                    startActivity(new Intent(context,SignIn_Activity.class));
                }

                finish();
            }
        },2000);
    }
}
