package com.wolfsoft.oleg.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.adapters.Favorite_Video_Adapter;
import com.wolfsoft.oleg.bean.Featured_Video_Data;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.NetworkConnection;
import com.wolfsoft.oleg.utils.VideoPlayerConfig;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Video_Player_Activity extends AppCompatActivity implements Player.EventListener , View.OnClickListener,SeekBar.OnSeekBarChangeListener {

    Video_Player_Activity activity;
    PlayerView videoFullScreenPlayer;
    ProgressBar spinnerVideoDetails;
    ImageView imageViewExit,iv_play,iv_pause,iv_next,iv_previous,iv_fav;
    TextView tv_start_time,tv_end_time;
    SeekBar seekBar;

    String videoUri, commingFrom, strVideoList,header;

    SimpleExoPlayer player;
    MediaSource videoSource;
    boolean isPlayComplete = false;
    Gson gson = new Gson();

    int position = 0,totalLenth = 0 ,media_id = 0;
    List<Featured_Video_Data> dataList;
    RecyclerView recyclerView;
    Favorite_Video_Adapter adapter;
    Handler mHandler = new Handler();
    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (player != null) {
                int curretnPos = (int) player.getCurrentPosition();
                seekBar.setProgress(curretnPos);
                seekBar.setMax((int) player.getDuration());

                tv_start_time.setText(getCurrentTime(curretnPos));
                tv_end_time.setText(getCurrentTime(player.getDuration()));
                mHandler.postDelayed(mRunnable,1000);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video__player);
        activity = Video_Player_Activity.this;
        initComponent();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        header = ConstantMember.getHeader(activity);

        if (getIntent() != null) {
            position = getIntent().getIntExtra("position",0);
            commingFrom = getIntent().getStringExtra("comming_from");
            if (commingFrom.equals("home")) {
                strVideoList = ConstantMember.getValueFromSession(activity, "featured_video_list");
                if (!strVideoList.equals("")) {
                   dataList = gson.fromJson(strVideoList,new TypeToken<List<Featured_Video_Data>>(){}.getType());

                    if (dataList.size()>0) {
                        playFromAdapter(position);
                        //Call adapter if list not empty
                        callRecyclerAdapter();
                        checkIsFavorite();
                    }
                }
            } else {
                    strVideoList = ConstantMember.getValueFromSession(activity,"fav_video_list");
                    dataList = gson.fromJson(strVideoList,new TypeToken<List<Featured_Video_Data>>(){}.getType());
                    playFromAdapter(position);
                    callRecyclerAdapter();
            }
        }

    }


    public void checkIsFavorite() {
        if (dataList.size() > 0) {
            Log.e("isFav","select = "+dataList.get(position).getIs_favorite());
            if (dataList.get(position).getIs_favorite() == 1) {
                iv_fav.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_favorites_selected));
            } else {
                iv_fav.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_favorites));
            }
        }

    }

    public void initComponent() {

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false));

        videoFullScreenPlayer = findViewById(R.id.videoFullScreenPlayer);
        spinnerVideoDetails   = findViewById(R.id.spinnerVideoDetails);
        imageViewExit         = findViewById(R.id.imageViewExit);
        iv_play  =  videoFullScreenPlayer.findViewById(R.id.image_view_play);
        iv_pause =  videoFullScreenPlayer.findViewById(R.id.image_view_pause);
        iv_next  =  videoFullScreenPlayer.findViewById(R.id.image_view_next);
        iv_previous = videoFullScreenPlayer.findViewById(R.id.image_view_previous);
        iv_fav   = findViewById(R.id.image_view_favorite);

        tv_start_time = findViewById(R.id.text_start_time);
        tv_end_time   = findViewById(R.id.text_end_time);

        seekBar = findViewById(R.id.simpleSeekBar);
        seekBar.setProgress(0);
        seekBar.setMax(100);
        seekBar.setOnSeekBarChangeListener(this);

        //Set on click listener
        imageViewExit.setOnClickListener(this);
        iv_play.setOnClickListener(this);
        iv_pause.setOnClickListener(this);
        iv_next.setOnClickListener(this);
        iv_previous.setOnClickListener(this);
        iv_fav.setOnClickListener(this);
        videoFullScreenPlayer.setOnClickListener(this);
    }

    public void callRecyclerAdapter() {
        adapter = new Favorite_Video_Adapter(activity,dataList,"video_player");
        recyclerView.setAdapter(adapter);
    }



    public void playFromAdapter(int position) {
        this.position = position;
        if (dataList.size()>0) {
            totalLenth = dataList.size();
            videoUri = dataList.get(position).getFile_path();
            Log.e("vieo","play url = "+videoUri);
            setUp();
            player.prepare(videoSource);
            player.setPlayWhenReady(true);
            checkIsFavorite();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imageViewExit:
                 finish();
                break;
            case R.id.image_view_play:
                if (!isPlayComplete) {
                    player.setPlayWhenReady(true);
                    iv_play.setVisibility(View.GONE);
                    iv_pause.setVisibility(View.VISIBLE);
                } else {
                    setUp();
                    player.prepare(videoSource);
                    player.setPlayWhenReady(true);
                    iv_play.setVisibility(View.GONE);
                    iv_pause.setVisibility(View.GONE);
                }
                break;
            case R.id.image_view_pause:
                pausePlayer();
                break;
            case R.id.image_view_next:
                if(position != totalLenth - 1){
                    position = position+1;
                }
                if (position < totalLenth) {
                    videoUri = dataList.get(position).getFile_path();
                    setUp();
                    player.prepare(videoSource);
                    player.setPlayWhenReady(true);
                    iv_play.setVisibility(View.GONE);
                    iv_pause.setVisibility(View.INVISIBLE);
                }
                //Check for next track
                checkIsFavorite();
                break;
            case R.id.image_view_previous:
                if  (position != 0) {
                    position = position - 1;
                }

                if (position < totalLenth) {
                    videoUri = dataList.get(position).getFile_path();
                    setUp();
                    player.prepare(videoSource);
                    player.setPlayWhenReady(true);
                    iv_play.setVisibility(View.GONE);
                    iv_pause.setVisibility(View.INVISIBLE);
                }
                //Check for previous track
                checkIsFavorite();
                break;
            case R.id.image_view_favorite:

                media_id = dataList.get(position).getId();
                if (NetworkConnection.checkConnection(activity)) {
                     addToFav();
                } else {
                    ConstantMember.showMessage(activity,"No internet connection");
                }

                break;
            default:break;
        }
    }

    private void setUp() {
        initializePlayer();
        if (videoUri == null) {
            return;
        }
        buildMediaSource(Uri.parse(videoUri));
    }

    private void initializePlayer() {
        if (player == null) {
            // 1. Create a default TrackSelector
            LoadControl loadControl = new DefaultLoadControl(
                    new DefaultAllocator(true, 16),
                    VideoPlayerConfig.MIN_BUFFER_DURATION,
                    VideoPlayerConfig.MAX_BUFFER_DURATION,
                    VideoPlayerConfig.MIN_PLAYBACK_START_BUFFER,
                    VideoPlayerConfig.MIN_PLAYBACK_RESUME_BUFFER, -1, true);

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);
            videoFullScreenPlayer.setPlayer(player);

        }


    }

    private void buildMediaSource(Uri mUri) {
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
         videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        // Prepare the player with the source.

        player.setPlayWhenReady(false);
        player.addListener(this);

//        player.getCurrentPosition();
//        player.getDuration();
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void pausePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.getPlaybackState();
            iv_play.setVisibility(View.VISIBLE);
            iv_pause.setVisibility(View.GONE);
        }
    }

    private void resumePlayer() {
        if (player != null) {
            if (!isPlayComplete) {
                player.setPlayWhenReady(true);
                player.getPlaybackState();
                iv_play.setVisibility(View.GONE);
                iv_pause.setVisibility(View.INVISIBLE
                );
            } else {
                setUp();
                player.prepare(videoSource);
                player.setPlayWhenReady(true);
                iv_play.setVisibility(View.GONE);
                iv_pause.setVisibility(View.INVISIBLE);
            }
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        pausePlayer();
        if (mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        resumePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }


    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {
        Log.e("vieo","timeline= ");
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if(playbackState == Player.STATE_ENDED){
            // your brain put here
            isPlayComplete = true;
            iv_pause.setVisibility(View.INVISIBLE);
            iv_play.setVisibility(View.VISIBLE);
              mHandler.removeCallbacks(mRunnable);
        }

        if (playbackState == Player.STATE_BUFFERING){
            spinnerVideoDetails.setVisibility(View.VISIBLE);
            iv_play.setVisibility(View.INVISIBLE);
            iv_pause.setVisibility(View.INVISIBLE);
            iv_next.setVisibility(View.INVISIBLE);
            iv_previous.setVisibility(View.INVISIBLE);
            mHandler.removeCallbacks(mRunnable);
        } else {
            Log.e("video","isPlaying= ");
            spinnerVideoDetails.setVisibility(View.INVISIBLE);
            iv_play.setVisibility(View.INVISIBLE);
            iv_pause.setVisibility(View.VISIBLE);
            iv_next.setVisibility(View.VISIBLE);
            iv_previous.setVisibility(View.VISIBLE);
            mHandler.postDelayed(mRunnable,1000);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    //public void add to favorite
    public void addToFav() {
        Api.getClient().addToFavorite(header,""+media_id).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        if (response.body().isIs_favorite()) {
                           iv_fav.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_favorites_selected));
                        } else {
                            iv_fav.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_favorites));
                        }
                        ConstantMember.setBooleanValueInSession(activity,"is_update",true);
                        ConstantMember.showMessage(activity,response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                ConstantMember.showMessage(activity,"Something went wrong");
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // In landscape
            Log.e("screen","landscape = ");
        } else {
            // In portrait
            Log.e("screen","Portrait");
        }
    }


    public String getCurrentTime(long ms) {
        ms /= 1000;
        return (ms / 3600) + ":" + ((ms % 3600) / 60) + ":" + ((ms % 3600) % 60);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
