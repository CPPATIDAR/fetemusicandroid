package com.wolfsoft.oleg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Audio_Player_Activity;
import com.wolfsoft.oleg.bean.Featured_Audio_Data;
import com.wolfsoft.oleg.utils.ConstantMember;

import java.util.List;

public class Artist_List_Detail_Adapter extends RecyclerView.Adapter<Artist_List_Detail_Adapter.ViewHolder> {


    private Context context;
    private List<Featured_Audio_Data> dataList;
    Gson gson = new Gson();

    public Artist_List_Detail_Adapter(Context context, List<Featured_Audio_Data> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public Artist_List_Detail_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_item_artist_detail,viewGroup,false);

        return new Artist_List_Detail_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Artist_List_Detail_Adapter.ViewHolder viewHolder, final int position) {

        viewHolder.tv_id.setText(dataList.get(position).getTitle());


        Log.e("list_name","id=="+dataList.get(position).getTitle());


        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str_list = gson.toJson(dataList);
                ConstantMember.setValueInSession(context,"user_playlist",str_list);
                Intent intent = new Intent(context, Audio_Player_Activity.class);
                intent.putExtra("position",position);
                intent.putExtra("comming_from","");

                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    class ViewHolder extends  RecyclerView.ViewHolder {
        TextView tv_id;
        LinearLayout linearLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_id = itemView.findViewById(R.id.text_song_name);
            linearLayout = itemView.findViewById(R.id.linear_layout);
        }
    }
}
