package com.wolfsoft.oleg.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Dialog_Playlist_Activity;
import com.wolfsoft.oleg.bean.PlayListData;
import com.wolfsoft.oleg.utils.ConstantMember;

import java.util.List;

public class Dialog_List_Adapter extends RecyclerView.Adapter<Dialog_List_Adapter.ViewHolder> {

    private Context context;
    private Dialog_Playlist_Activity activity;
    private List<PlayListData> dataList;
    public Dialog_List_Adapter(Context context,List<PlayListData> dataList){
        this.context  = context;
        this.dataList = dataList;
        this.activity = (Dialog_Playlist_Activity) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_list_dialog,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {
        viewHolder.textView_Name.setText(dataList.get(position).getTitle());

        viewHolder.textView_Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.addToPlayList(""+dataList.get(position).getId());
            }
        });

        viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.addToPlayList(""+dataList.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView_Name;
        RelativeLayout relativeLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView_Name  = itemView.findViewById(R.id.text_name);
            relativeLayout = itemView.findViewById(R.id.relative);
         }
    }
}
