package com.wolfsoft.oleg.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Audio_Player_Activity;
import com.wolfsoft.oleg.bean.Featured_Audio_Data;
import com.wolfsoft.oleg.utils.ConstantMember;

import java.util.List;

public class Favorite_Audio_Adapter extends RecyclerView.Adapter<Favorite_Audio_Adapter.ViewHolder> {

    private Context context;
    private List<Featured_Audio_Data> dataList;

    private Gson  gson = new Gson();

    public Favorite_Audio_Adapter(Context context,List<Featured_Audio_Data> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_audio_sogns,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {

        viewHolder.tv_artis_name.setText(dataList.get(position).getArtist_name());
        viewHolder.tv_song_name.setText(dataList.get(position).getTitle());
        viewHolder.tv_genre.setText(dataList.get(position).getGenre());
        viewHolder.tv_release_date.setText(dataList.get(position).getReleased_date());

        if (dataList.get(position).getFile_image() !=null) {
            Picasso.get().load(dataList.get(position).getFile_image()).into(viewHolder.imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {

                }
            });
        }

        //Listener
        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str_list = gson.toJson(dataList);
                ConstantMember.setValueInSession(context,"user_playlist",str_list);
                Intent intent = new Intent(context, Audio_Player_Activity.class);
                intent.putExtra("position",position);
                intent.putExtra("comming_from","playlist");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_artis_name,tv_song_name,tv_release_date,tv_genre;
        ImageView imageView;
        LinearLayout linearLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_artis_name   = itemView.findViewById(R.id.text_artist_name);
            tv_song_name    = itemView.findViewById(R.id.text_song_name);
            tv_genre        = itemView.findViewById(R.id.text_genre);
            tv_release_date = itemView.findViewById(R.id.text_date);

            imageView = itemView.findViewById(R.id.image_view);
            linearLayout = itemView.findViewById(R.id.linear_layout);

        }
    }
}
