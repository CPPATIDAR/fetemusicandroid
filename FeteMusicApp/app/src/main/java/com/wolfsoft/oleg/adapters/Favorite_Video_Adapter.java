package com.wolfsoft.oleg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Video_Player_Activity;
import com.wolfsoft.oleg.bean.Featured_Video_Data;

import java.util.List;

public class Favorite_Video_Adapter extends RecyclerView.Adapter<Favorite_Video_Adapter.ViewHolder> {

    private Context context;
    private List<Featured_Video_Data> videoDataList;
    private String calledFrom;
    private Video_Player_Activity activity;

    public Favorite_Video_Adapter(Context context,List<Featured_Video_Data> videoDataList,String callFrom){
        this.calledFrom = callFrom;
        this.context = context;
        this.videoDataList = videoDataList;
        if (callFrom.equals("video_player")) {
            this.activity = (Video_Player_Activity) context;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view  ;
//                = LayoutInflater.from(context).inflate(R.layout.list_item_video_player, viewGroup, false);
        if (calledFrom.equals("video_player")) {
            view = LayoutInflater.from(context).inflate(R.layout.list_item_video_player, viewGroup, false);
        }
        else {
            view = LayoutInflater.from(context).inflate(R.layout.list_item_video_songs,viewGroup,false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        viewHolder.tv_artist_name.setText(videoDataList.get(position).getArtist_name());
        viewHolder.tv_title.setText(videoDataList.get(position).getTitle());

        if (videoDataList.get(position).getFile_image() !=null) {
            Picasso.get().load(videoDataList.get(position).getFile_image()).into(viewHolder.imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {

                }
            });
        }

        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (calledFrom.equals("video_player")) {
                    activity.playFromAdapter(position);
                } else {
                    Intent intent = new Intent(context, Video_Player_Activity.class);
                    intent.putExtra("position",position);
                    intent.putExtra("comming_from","fav_video");
                    context.startActivity(intent);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return videoDataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tv_artist_name,tv_title;
        LinearLayout linearLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_view);
            tv_artist_name = itemView.findViewById(R.id.text_artist_name);
            tv_title = itemView.findViewById(R.id.text_title);
            linearLayout = itemView.findViewById(R.id.linear);
        }
    }
}
