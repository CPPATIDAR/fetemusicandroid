package com.wolfsoft.oleg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Audio_Player_Activity;
import com.wolfsoft.oleg.bean.Featured_Audio_Data;

import java.util.List;

public class Featured_Song_Adapter extends RecyclerView.Adapter<Featured_Song_Adapter.ViewHolder> {

    private Context context;
    private List<Featured_Audio_Data> dataList;
    public Featured_Song_Adapter(Context context,List<Featured_Audio_Data> dataList){
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_featured_songs,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        if (dataList.get(position).getFile_image() != null) {
            Picasso.get().load(""+dataList.get(position).getFile_image()).into(viewHolder.imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {

                }
            });

        }

        //Init listener
        viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Audio_Player_Activity.class);
                intent.putExtra("position",position);
                intent.putExtra("comming_from","home");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        RelativeLayout relativeLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_view);
            relativeLayout = itemView.findViewById(R.id.relative_layout);
        }
    }
}
