package com.wolfsoft.oleg.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.bean.LiveAudioData;

import java.util.List;

public class Live_Audio_Adapter extends RecyclerView.Adapter<Live_Audio_Adapter.ViewHolder> {

    private Context context;
    private List<LiveAudioData> dataList;

    public Live_Audio_Adapter(Context context, List<LiveAudioData> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_item_audio_sogns,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class ViewHolder extends  RecyclerView.ViewHolder {
       ViewHolder(@NonNull View itemView) {
           super(itemView);
       }
   }
}


