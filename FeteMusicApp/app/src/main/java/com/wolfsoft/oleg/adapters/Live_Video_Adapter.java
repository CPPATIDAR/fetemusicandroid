package com.wolfsoft.oleg.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.LivePlayerActivity;
import com.wolfsoft.oleg.bean.LiveVdeoData;

import org.w3c.dom.Text;

import java.util.List;

public class Live_Video_Adapter extends RecyclerView.Adapter <Live_Video_Adapter.ViewHolder>{

    private Context context;
    private List<LiveVdeoData> dataList;

    public Live_Video_Adapter(Context context,List<LiveVdeoData> data) {
        this.context = context;
        this.dataList = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_item_live,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {

        viewHolder.tv_user_name.setText(dataList.get(position).getUsername());
        viewHolder.tv_status.setText(dataList.get(position).getType());

        String url = dataList.get(position).getImagePath();
        if (!url.isEmpty()) {
            Picasso.get().load(url).into(viewHolder.image, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {

                }
            });
        }


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, LivePlayerActivity.class);
                intent.putExtra("resourceUri",dataList.get(position).getResourceUrl());
                Log.e("sending data ", "through intent resourceUri = " + dataList.get(position).getResourceUrl());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder {
        ImageView image;
        TextView tv_status,tv_user_name;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view);
            tv_status = itemView.findViewById(R.id.text_status);
            tv_user_name = itemView.findViewById(R.id.text_name);
        }
    }
}
