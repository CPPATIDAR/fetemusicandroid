package com.wolfsoft.oleg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Audio_Player_Activity;
import com.wolfsoft.oleg.activity.PlayList_Detail_Activity;
import com.wolfsoft.oleg.bean.PlayListData;

import java.util.List;

public class Playlist_Adapter extends RecyclerView.Adapter<Playlist_Adapter.ViewHolder> {

    private Context context;
    private List<PlayListData> dataList;

    public Playlist_Adapter(Context context, List<PlayListData> data) {
        this.context = context;
        this.dataList = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_item_playlist_created,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        if (dataList.get(position).getTitle() !=null) {
          viewHolder.textView_Title.setText(dataList.get(position).getTitle());
        }

        viewHolder.textView_Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNext(""+dataList.get(position).getId());
            }
        });

        viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               goToNext(""+dataList.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private void goToNext(String id) {
        Intent intent = new Intent(context, PlayList_Detail_Activity.class);
        intent.putExtra("playlist_id",id);
        context.startActivity(intent);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView_Title;
        RelativeLayout relativeLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView_Title = itemView.findViewById(R.id.text_name);
            relativeLayout = itemView.findViewById(R.id.relative);
        }
    }
}
