package com.wolfsoft.oleg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.PlayList_Detail_Activity;
import com.wolfsoft.oleg.bean.PlayListData;

import java.util.List;

public class Recycler_Featured_Playlist extends RecyclerView.Adapter<Recycler_Featured_Playlist.ViewHolder> {

    Context context;
    private List<PlayListData> dataList;
    private LayoutInflater inflater;

    public Recycler_Featured_Playlist(Context context,List<PlayListData> data) {
        this.context = context;
        this.dataList = data;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = inflater.inflate(R.layout.list_item_featured_playlist,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        viewHolder.tv_title.setText(dataList.get(position).getTitle());

        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNext(""+dataList.get(position).getId());
            }
        });
    }

    private void goToNext(String id) {
        Intent intent = new Intent(context, PlayList_Detail_Activity.class);
        intent.putExtra("playlist_id",id);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title;
        LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.text_title);
            linearLayout=itemView.findViewById(R.id.linear_layout);
        }
    }
}
