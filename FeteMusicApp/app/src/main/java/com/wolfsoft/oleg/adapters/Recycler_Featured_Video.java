package com.wolfsoft.oleg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Video_Player_Activity;
import com.wolfsoft.oleg.bean.Featured_Video_Data;

import java.util.List;

public class Recycler_Featured_Video extends RecyclerView.Adapter<Recycler_Featured_Video.ViewHolder> {

    private Context context;
    private List<Featured_Video_Data> dataList;
    public Recycler_Featured_Video(Context context, List<Featured_Video_Data> data){
       this.context = context;
       this.dataList = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_item_featured_video,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
          viewHolder.tv_title.setText(dataList.get(position).getTitle());
//        viewHolder.tv_sub_title.setText(dataList.get(position).getSubTitle());
          if (dataList.get(position).getFile_image() !=null) {
              Picasso.get().load(""+dataList.get(position).getFile_image()).into(viewHolder.imageView, new Callback() {
                  @Override
                  public void onSuccess() {

                  }

                  @Override
                  public void onError(Exception e) {

                  }
              });
          }

          //Click listener
          viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  Intent intent = new Intent(context, Video_Player_Activity.class);
                  intent.putExtra("position",position);
                  intent.putExtra("comming_from","home");
                  context.startActivity(intent);
              }
          });

//        featured_video_list
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_title,tv_sub_title;
        ImageView imageView;
        RelativeLayout relativeLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_title  = itemView.findViewById(R.id.text_title);
            tv_sub_title = itemView.findViewById(R.id.text_sub_title);
            imageView = itemView.findViewById(R.id.image_view);
            relativeLayout = itemView.findViewById(R.id.relative);
        }
    }
}
