package com.wolfsoft.oleg.adapters;

import android.content.Context;
import android.content.Intent;
import android.icu.util.ValueIterator;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.PlayList_Detail_Activity;
import com.wolfsoft.oleg.bean.PlayListData;

import java.util.List;

public class SearchAdapterPlaylist extends RecyclerView.Adapter<SearchAdapterPlaylist.ViewHolder> {

    private Context context;
    private List<PlayListData> dataList;
    private LayoutInflater inflater;

    public SearchAdapterPlaylist(Context context,List<PlayListData> data) {
        this.context = context;
        this.dataList = data;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = inflater.inflate(R.layout.search_item_playlist,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        viewHolder.tv_title.setText(dataList.get(position).getTitle());


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNext(""+dataList.get(position).getId());
            }
        });



    }

    private void goToNext(String id) {
        Intent intent = new Intent(context, PlayList_Detail_Activity.class);
        intent.putExtra("playlist_id",id);
        context.startActivity(intent);
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.text_title);
            imageView = itemView.findViewById(R.id.image_view);
        }
    }
}
