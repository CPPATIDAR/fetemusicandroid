package com.wolfsoft.oleg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Video_Player_Activity;
import com.wolfsoft.oleg.bean.Featured_Video_Data;
import com.wolfsoft.oleg.utils.ConstantMember;

import java.util.List;

public class SearchAdapterVideo extends RecyclerView.Adapter<SearchAdapterVideo.ViewHolder> {

    Context context;
    private List<Featured_Video_Data> dataList;
    private LayoutInflater inflater;
    private Gson gson =new Gson();

    public SearchAdapterVideo(Context context,List<Featured_Video_Data> data) {
        this.context = context;
        this.dataList = data;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = inflater.inflate(R.layout.search_item_video,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        viewHolder.tv_title.setText(dataList.get(position).getTitle());

        if (dataList.get(position).getFile_image() != null) {
            Picasso.get().load(""+dataList.get(position).getFile_image()).into(viewHolder.imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {

                }
            });
        }


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str_list = gson.toJson(dataList);
                ConstantMember.setValueInSession(context,"featured_video_list",str_list);
                Intent intent = new Intent(context, Video_Player_Activity.class);
                intent.putExtra("position",position);
                intent.putExtra("comming_from","home");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder {
        TextView tv_title;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.text_title);
            imageView = itemView.findViewById(R.id.image_view);
        }
    }
}
