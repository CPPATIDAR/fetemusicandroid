package com.wolfsoft.oleg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Artist_List_Activity;
import com.wolfsoft.oleg.bean.ArtistData;

import java.util.List;

public class SearchAdpaterArtist extends RecyclerView.Adapter<SearchAdpaterArtist.ViewHolder> {

    private Context context;
    private List<ArtistData> dataList;
    private LayoutInflater inflater;

    public SearchAdpaterArtist(Context context,List<ArtistData> data) {
        this.context = context;
        this.dataList = data;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = inflater.inflate(R.layout.search_item_artist,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        viewHolder.tv_artist_name.setText(dataList.get(position).getName());

        if (dataList.get(position).getArtist_image() != null) {
            Picasso.get().load(""+dataList.get(position).getArtist_image()).into(viewHolder.iv_image, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {

                }
            });
        }


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Artist_List_Activity.class);
                intent.putExtra("position",position);
                intent.putExtra("artist_id",""+dataList.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_artist_name;
        ImageView iv_image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_artist_name = itemView.findViewById(R.id.text_artist_name);
            iv_image = itemView.findViewById(R.id.image_view);
        }
    }
}
