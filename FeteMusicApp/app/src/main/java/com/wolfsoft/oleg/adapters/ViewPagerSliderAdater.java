package com.wolfsoft.oleg.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Artist_List_Activity;
import com.wolfsoft.oleg.bean.ArtistData;

import java.util.List;

public class ViewPagerSliderAdater extends PagerAdapter {

    Context context;
    private List<ArtistData> dataList;
//    private LayoutInflater inflater;

    public ViewPagerSliderAdater(Context context, List<ArtistData> data) {
        this.context = context;
        this.dataList = data;
//        inflater = LayoutInflater.from(context);
    }

    @Override

    public int getCount(){
        return dataList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        Log.e("isViewFromObj==",(view==obj)+"");
        return view==obj;

    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        @SuppressLint("InflateParams")
        View customview = LayoutInflater.from(context).inflate(R.layout.list_item_viewpager,null);
       ImageView imageView = customview.findViewById(R.id.image_view);
        if (dataList.get(position).getArtist_image() != null) {
//
            try {
                Log.e("inside adapter","url = "+dataList.get(position).getArtist_image()+"imageView= "+imageView);
                Picasso.get().load(dataList.get(position).getArtist_image()).into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("inside adapter","error = "+e);
                    }
                });

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, Artist_List_Activity.class);
                intent.putExtra("position",position);
                intent.putExtra("artist_id",""+dataList.get(position).getId());
                context.startActivity(intent);
            }
        });
        container.addView(customview);
        return customview;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView(container);
    }
}
