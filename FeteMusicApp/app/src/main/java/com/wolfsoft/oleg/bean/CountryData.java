package com.wolfsoft.oleg.bean;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CountryData {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public ArrayList<CountryData> dataArrayPrtnerList;

}
