package com.wolfsoft.oleg.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CountryList {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("countries")
    private ArrayList<CountryData> countrieList;


    public ArrayList<CountryData> getCountrieList() {
        return countrieList;
    }

    public void setCountrieList(ArrayList<CountryData> countrieList) {
        this.countrieList = countrieList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
