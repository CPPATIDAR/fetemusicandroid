package com.wolfsoft.oleg.bean;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FeaturedData {

    @SerializedName("artists")
    private ArrayList<ArtistData> artistDataList;

    @SerializedName("audios")
    private ArrayList<Featured_Audio_Data> featuredSongList;

    @SerializedName("videos")
    private ArrayList<Featured_Video_Data> featuredVideoList;

    @SerializedName("playlists")
    private ArrayList<PlayListData> playListData;

    public ArrayList<Featured_Audio_Data> getFeaturedSongList() {
        return featuredSongList;
    }

    public void setFeaturedSongList(ArrayList<Featured_Audio_Data> featuredSongList) {
        this.featuredSongList = featuredSongList;
    }

    public ArrayList<ArtistData> getArtistDataList() {
        return artistDataList;
    }

    public void setArtistDataList(ArrayList<ArtistData> artistDataList) {
        this.artistDataList = artistDataList;
    }

    public ArrayList<Featured_Video_Data> getFeaturedVideoList() {
        return featuredVideoList;
    }

    public void setFeaturedVideoList(ArrayList<Featured_Video_Data> featuredVideoList) {
        this.featuredVideoList = featuredVideoList;
    }

    public ArrayList<PlayListData> getPlayListData() {
        return playListData;
    }

    public void setPlayListData(ArrayList<PlayListData> playListData) {
        this.playListData = playListData;
    }
}
