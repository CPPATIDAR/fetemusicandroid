package com.wolfsoft.oleg.bean;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FeaturedResponse {

    @SerializedName("status")
    private boolean status;

    @SerializedName("response")
    private FeaturedData featuredData;

    @SerializedName("medias")
    private
    ArrayList<Featured_Video_Data> mediaList;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public FeaturedData getFeaturedData() {
        return featuredData;
    }

    public void setFeaturedData(FeaturedData featuredData) {
        this.featuredData = featuredData;
    }

    public ArrayList<Featured_Video_Data> getMediaList() {
        return mediaList;
    }

    public void setMediaList(ArrayList<Featured_Video_Data> mediaList) {
        this.mediaList = mediaList;
    }
}
