package com.wolfsoft.oleg.bean;

import com.google.gson.annotations.SerializedName;

public class ForgotData {

    @SerializedName("user_id")
    private int user_id;

    @SerializedName("forget_token")
    private int forget_token;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getForget_token() {
        return forget_token;
    }

    public void setForget_token(int forget_token) {
        this.forget_token = forget_token;
    }
}
