package com.wolfsoft.oleg.bean;

import com.google.gson.annotations.SerializedName;

public class PlayListMediaData {

    @SerializedName("id")
    private  int id;

    @SerializedName("artist_id")
    private  int artist_id;

    @SerializedName("media_type")
    private  String media_type;

    @SerializedName("file_type")
    private  String file_type;

    @SerializedName("title")
    private  String title;

    @SerializedName("description")
    private  String description;

    @SerializedName("genre")
    private  String genre;

    @SerializedName("released_date")
    private  String released_date;

    @SerializedName("file_image")
    private  String file_image;

    @SerializedName("file_path")
    private  String file_path;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(int artist_id) {
        this.artist_id = artist_id;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleased_date() {
        return released_date;
    }

    public void setReleased_date(String released_date) {
        this.released_date = released_date;
    }

    public String getFile_image() {
        return file_image;
    }

    public void setFile_image(String file_image) {
        this.file_image = file_image;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }
}
