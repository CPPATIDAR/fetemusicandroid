package com.wolfsoft.oleg.bean;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SignUpResponse {

    @SerializedName("status")
    private boolean status;

    @SerializedName("message")
    private String message;

    @SerializedName("user")
    private UserLoginData userData;

    @SerializedName("response")
    private ForgotData forgotData;

    @SerializedName("access_token")
    private String access_token;

    @SerializedName("token_type")
    private String token_type;

    @SerializedName("is_favorite")
    private boolean is_favorite;

    @SerializedName("playlists")
    private
    ArrayList<PlayListData> playList;

    @SerializedName("medias")
    private
    ArrayList<Featured_Audio_Data> mediaList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public UserLoginData getUserData() {
        return userData;
    }

    public void setUserData(UserLoginData userData) {
        this.userData = userData;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public ForgotData getForgotData() {
        return forgotData;
    }

    public void setForgotData(ForgotData forgotData) {
        this.forgotData = forgotData;
    }

    public ArrayList<PlayListData> getPlayList() {
        return playList;
    }

    public ArrayList<Featured_Audio_Data> getMediaList() {
        return mediaList;
    }

    public void setMediaList(ArrayList<Featured_Audio_Data> mediaList) {
        this.mediaList = mediaList;
    }

    public boolean isIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(boolean is_favorite) {
        this.is_favorite = is_favorite;
    }
}
