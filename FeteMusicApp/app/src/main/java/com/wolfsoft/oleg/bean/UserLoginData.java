package com.wolfsoft.oleg.bean;

import com.google.gson.annotations.SerializedName;

public class UserLoginData {

//                    "id": 54,
//                    "name": "Rituraj Gurjar",
//                    "email": "rituraj@gmail.com",
//                    "email_verified_at": null,
//                    "relationship_status": "Single",
//                    "age": 28,
//                    "favorite_soca_genre": "Destra Garcia",
//                    "country_id": 91,
//                    "profile_picture": null,
//                    "forget_token": null,
//                    "is_active": 1,
//                    "device_token": "",
//                    "device_type": null,
//                    "created_at": "2019-06-06 12:46:47",
//                    "updated_at": "2019-06-06 12:47:06",
//                    "role_type": "customer",
//                    "country_name": "",
//                    "country": null

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

//    @SerializedName("email_verified_at")
//    private boolean email_verified_at;

    @SerializedName("relationship_status")
    private String relationship_status;

    @SerializedName("age")
    private int age;

    @SerializedName("favorite_soca_genre")
    private String favorite_soca_genre;

    @SerializedName("country_name")
    private String  country_name;


    @SerializedName("country_id")
    private int country_id;

    @SerializedName("profile_picture")
    private String profile_picture;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRelationship_status() {
        return relationship_status;
    }

    public void setRelationship_status(String relationship_status) {
        this.relationship_status = relationship_status;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public String getFavorite_soca_genre() {
        return favorite_soca_genre;
    }

    public void setFavorite_soca_genre(String favorite_soca_genre) {
        this.favorite_soca_genre = favorite_soca_genre;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }
}
