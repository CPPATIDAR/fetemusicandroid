package com.wolfsoft.oleg.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Home_Activity;
import com.wolfsoft.oleg.activity.Search_Activity;
import com.wolfsoft.oleg.adapters.Featured_Song_Adapter;
import com.wolfsoft.oleg.adapters.Recycler_Featured_Playlist;
import com.wolfsoft.oleg.adapters.Recycler_Featured_Video;
import com.wolfsoft.oleg.adapters.ViewPagerSliderAdater;
import com.wolfsoft.oleg.bean.ArtistData;
import com.wolfsoft.oleg.bean.FeaturedResponse;
import com.wolfsoft.oleg.bean.Featured_Audio_Data;
import com.wolfsoft.oleg.bean.Featured_Video_Data;
import com.wolfsoft.oleg.bean.PlayListData;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Featured_Artist_Fragment extends Fragment {

    String headers,strListAudioSong,strListVidoeSong,strArtistData;
    RecyclerView recyclerView_Featured_Video,recyclerViewFeatureSong,recyclerView_Playlist;
    Home_Activity activity;

    ViewPager viewPager;

    ViewPagerSliderAdater viewPagerAdater;
    Recycler_Featured_Video recycler_fetured_video;
    Featured_Song_Adapter featured_song_adapter;
    Recycler_Featured_Playlist adapterFeaturedPlaylist;

    List<ArtistData> listArtistData = new ArrayList<>();
    List<Featured_Video_Data> listFeaturedVideo = new ArrayList<>();
    List<Featured_Audio_Data> listFeaturedSong = new ArrayList<>();
    List<PlayListData> listPlaylist = new ArrayList<>();
    Gson gson = new Gson();

    LinearLayout linear_search;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_featured__artist, container, false);
        activity = (Home_Activity) getActivity();

        initComponent(view);
        //call featured video list
        callFeaturedSong();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConstantMember.getBooleanFromSession(activity,"is_update")) {
            callFeaturedSong();
            ConstantMember.setBooleanValueInSession(activity,"is_update",false);
        }
    }

    public void initComponent(View view) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        recyclerView_Featured_Video = view.findViewById(R.id.recycler_featured_video);
        recyclerView_Featured_Video.setLayoutManager(layoutManager);

        recyclerViewFeatureSong = view.findViewById(R.id.recycler_featured_song);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewFeatureSong.setLayoutManager(layoutManager1);

        recyclerView_Playlist = view.findViewById(R.id.recycler_featured_playlist);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        recyclerView_Playlist.setLayoutManager(layoutManager2);

        //Find the viewpager
        viewPager = view.findViewById(R.id.pager_feature);

        linear_search = view.findViewById(R.id.linear_ic_search);
        linear_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, Search_Activity.class));
            }
        });
    }


    public void callFeaturedSong(){
        headers = ConstantMember.getHeader(activity);
        Log.e("featured","get header = "+headers);
        DialogsUtils.showProgressDialog(activity,"Please wait...");
        Api.getClient().getFeaturedList(headers,"").enqueue(new Callback<FeaturedResponse>() {
            @Override
            public void onResponse(Call<FeaturedResponse> call, Response<FeaturedResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        listArtistData    = response.body().getFeaturedData().getArtistDataList();
                        listFeaturedSong  = response.body().getFeaturedData().getFeaturedSongList();
                        listFeaturedVideo = response.body().getFeaturedData().getFeaturedVideoList();
                        listPlaylist      = response.body().getFeaturedData().getPlayListData();
                        Log.e("featured","audio list size = "+response.body().getFeaturedData().getPlayListData().size());
                        callAdapterSlider();
                        callAdapterVideo();
                        callAdapterAudio();
                        callAdapterPlayList();
                        DialogsUtils.dismissDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeaturedResponse> call, Throwable t) {
                DialogsUtils.dismissDialog();
                Log.e("featured","audio error = "+t);

                strArtistData = ConstantMember.getValueFromSession(activity,"list_artist_data");
                if (!strArtistData.equals("")) {
                    listArtistData = gson.fromJson(strArtistData,new TypeToken<List<ArtistData>>(){}.getType());
                    callAdapterSlider();
                }

                strListAudioSong = ConstantMember.getValueFromSession(activity,"featured_audio_list");
                if (!strListAudioSong.equals("")) {
                     listFeaturedSong = gson.fromJson(strListAudioSong, new TypeToken<List<Featured_Audio_Data>>(){}.getType());
                     callAdapterAudio();
                }

                strListVidoeSong = ConstantMember.getValueFromSession(activity,"featured_video_list");
                if (!strListVidoeSong.equals("")) {
                   listFeaturedVideo =  gson.fromJson(strListVidoeSong, new TypeToken<List<Featured_Video_Data>>(){}.getType());
                   callAdapterVideo();
                }
            }
        });
    }

    public void callAdapterSlider() {
        if (listArtistData.size() > 0) {
            strArtistData = gson.toJson(listArtistData);
            ConstantMember.setValueInSession(activity,"list_artist_data",strArtistData);
            viewPagerAdater = new ViewPagerSliderAdater(activity,listArtistData);
            viewPager.setAdapter(viewPagerAdater);
        }
    }

    public void callAdapterVideo(){
        if (listFeaturedVideo.size()>0) {
            //store list in session
            strListVidoeSong  = gson.toJson(listFeaturedVideo);
            ConstantMember.setValueInSession(activity,"featured_video_list",""+strListVidoeSong);

            recycler_fetured_video = new Recycler_Featured_Video(activity,listFeaturedVideo);
            recyclerView_Featured_Video.setHasFixedSize(true);
            recyclerView_Featured_Video.setAdapter(recycler_fetured_video);
        }
    }

    public void callAdapterAudio() {
        if (listFeaturedSong.size()>0) {
            //store list in session
            strListAudioSong  = gson.toJson(listFeaturedSong);
            ConstantMember.setValueInSession(activity,"featured_audio_list",""+strListAudioSong);

            featured_song_adapter = new Featured_Song_Adapter(activity,listFeaturedSong);
            recyclerViewFeatureSong.setHasFixedSize(true);
            recyclerViewFeatureSong.setAdapter(featured_song_adapter);
        }
    }

    public void callAdapterPlayList() {
        if (listPlaylist.size() > 0) {
            adapterFeaturedPlaylist = new Recycler_Featured_Playlist(activity,listPlaylist);
            recyclerView_Playlist.setHasFixedSize(true);
            recyclerView_Playlist.setAdapter(adapterFeaturedPlaylist);
        }
    }
}
