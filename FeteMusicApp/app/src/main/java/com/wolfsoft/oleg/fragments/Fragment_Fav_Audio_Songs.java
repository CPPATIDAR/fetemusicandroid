package com.wolfsoft.oleg.fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.adapters.Favorite_Audio_Adapter;
import com.wolfsoft.oleg.bean.FavoriteAudioData;
import com.wolfsoft.oleg.bean.Featured_Audio_Data;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;
import com.wolfsoft.oleg.utils.NetworkConnection;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Fav_Audio_Songs extends Fragment {

    Context context;
    String  header,
            str_type = "audio",
            artist_id ="0";
    int page = 1,is_favorite = 1;

    List<Featured_Audio_Data> audioDataList = new ArrayList<>();
    RecyclerView recyclerView;
    Favorite_Audio_Adapter audio_adapter;
    Handler handler = new Handler();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment__audio__songs, container, false);
        //Find the views from view
        context = getActivity();
        header = ConstantMember.getHeader(context);

        recyclerView = view.findViewById(R.id.recycler);
        LinearLayoutManager manager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);

        if (NetworkConnection.checkConnection(context)) {
            getMedia();
        } else {
            ConstantMember.showMessage(context,"No internet connection");
        }

        return view;
    }

    public void getMedia(){
        DialogsUtils.showProgressDialog(context,"Please wait...");
        Api.getClient().getMedia(header,str_type,""+is_favorite,artist_id,""+page).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, final Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                audioDataList = response.body().getMediaList();
                                callFavAdapter();
                                DialogsUtils.dismissDialog();
                            }
                        },1000);
                    }else {
                        DialogsUtils.showProgressDialog(context,"Please wait...");
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
               DialogsUtils.dismissDialog();
                Log.e("error","on fav= "+t);
            }
        });
    }

    public void callFavAdapter() {
        if (audioDataList.size()>0) {
            //call adapter
            audio_adapter = new Favorite_Audio_Adapter(context,audioDataList);
            recyclerView.setAdapter(audio_adapter);
        }
    }
}
