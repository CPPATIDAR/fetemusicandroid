package com.wolfsoft.oleg.fragments;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.adapters.Favorite_Video_Adapter;
import com.wolfsoft.oleg.bean.FeaturedResponse;
import com.wolfsoft.oleg.bean.Featured_Video_Data;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Fav_Video_Songs extends Fragment {

    Context context;
    String  header,
            str_type = "video",
            artist_id ="0";
    int page = 1,is_favorite = 1;

    List<Featured_Video_Data> videoDataList = new ArrayList<>();
    RecyclerView recyclerView;
    Favorite_Video_Adapter video_adapter;
    Gson gson = new Gson();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment__video__songs, container, false);
        context = getActivity();
        header = ConstantMember.getHeader(context);

        //Find the view from inflate view
        recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(context,2));

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Do your Work
            getVideoMedia();
        }
    }

    public void getVideoMedia() {
        DialogsUtils.showProgressDialog(context,"Please wait...");
        Api.getClient().getVideoMedia(header,str_type,""+is_favorite,artist_id,""+page).enqueue(new Callback<FeaturedResponse>() {
            @Override
            public void onResponse(Call<FeaturedResponse> call, Response<FeaturedResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {

                        videoDataList = response.body().getMediaList();
                        callAdapter();

                        DialogsUtils.dismissDialog();

                    } else {
                        DialogsUtils.showProgressDialog(context,"Please wait...");
                    }
                }
            }

            @Override
            public void onFailure(Call<FeaturedResponse> call, Throwable t) {
                   DialogsUtils.dismissDialog();
            }
        });
    }

    public void callAdapter() {
        if (videoDataList.size() > 0) {

            String str_list = gson.toJson(videoDataList);
            ConstantMember.setValueInSession(context,"fav_video_list",str_list);

            video_adapter = new Favorite_Video_Adapter(context,videoDataList,"fav_video");
            recyclerView.setAdapter(video_adapter);
        }
    }
}
