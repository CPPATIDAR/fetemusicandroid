package com.wolfsoft.oleg.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.adapters.Live_Audio_Adapter;
import com.wolfsoft.oleg.bean.LiveAudioData;

import java.util.List;

public class Fragment_Live_Audio extends Fragment {

    Context context;
    List<LiveAudioData> liveAudioDataList;
    RecyclerView recyclerView;
    Live_Audio_Adapter live_audio_adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment__live__audio, container, false);
        context = getActivity();
        recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        live_audio_adapter = new Live_Audio_Adapter(context,liveAudioDataList);
        recyclerView.setAdapter(live_audio_adapter);

        return view;
    }

}
