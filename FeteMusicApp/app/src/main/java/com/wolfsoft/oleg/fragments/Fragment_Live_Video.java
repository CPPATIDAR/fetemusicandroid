package com.wolfsoft.oleg.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.activity.Broadcaster_Activity;
import com.wolfsoft.oleg.adapters.Live_Video_Adapter;
import com.wolfsoft.oleg.bean.LiveVdeoData;
import com.wolfsoft.oleg.utils.DialogsUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Fragment_Live_Video extends Fragment {


    private static final String API_KEY = "1q1slgn3yjb2jk6eszmrsmpah";
    private final OkHttpClient mOkHttpClient = new OkHttpClient();

    Context context;
    List<LiveVdeoData> videoDataList = new ArrayList<>();
    RecyclerView recyclerView;
    Live_Video_Adapter adapter;

    TextView textViewGoLive;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment__live__video, container, false);
        context = getActivity();

        recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        textViewGoLive = view.findViewById(R.id.golive);
        textViewGoLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, Broadcaster_Activity.class));
            }
        });

        getLatestResourceUri();

        return view;
    }


    private void getLatestResourceUri() {
        DialogsUtils.showProgressDialog(context, "Please wait...");
        Request request = new Request.Builder()
                .url("https://api.bambuser.com/broadcasts")
                .addHeader("Accept", "application/vnd.bambuser.v1+json")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer " + API_KEY)
                .get()
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(final Call call, final IOException e) {
                DialogsUtils.dismissDialog();
            }

            @Override
            public void onResponse(final Call call, final Response response) throws IOException {
                String resourceUri = null, imageUri = null;
                try {
                    String body = response.body().string();
                    JSONObject json = new JSONObject(body);
                    JSONArray results = json.getJSONArray("results");
                    Log.e("rj json ", "lenth = " + results);
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject latestBroadcast = results.optJSONObject(i);
                        imageUri = latestBroadcast.optString("preview");
                        resourceUri = latestBroadcast.optString("resourceUri");
                        LiveVdeoData model = new LiveVdeoData();
                        model.setImagePath(imageUri);
                        model.setResourceUrl(resourceUri);
                        model.setType(latestBroadcast.optString("type"));
                        model.setUsername(latestBroadcast.optString("title"));
                        videoDataList.add(model);
                    }
                    Log.e("rj json ", "lenth = " + videoDataList.size());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callAdapter();
                        }
                    });

                } catch (Exception ignored) {
                    DialogsUtils.dismissDialog();
                }
            }
        });


    }


    public void callAdapter() {
        if (videoDataList.size() > 0) {
            DialogsUtils.dismissDialog();
            adapter = new Live_Video_Adapter(context, videoDataList);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapter);
            Log.e("rj adapter ", "detail = " + adapter + "\n" + recyclerView);
        } else {
            DialogsUtils.dismissDialog();
        }
    }
}
