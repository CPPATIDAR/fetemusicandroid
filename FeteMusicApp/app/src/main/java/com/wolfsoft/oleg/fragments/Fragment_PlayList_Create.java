package com.wolfsoft.oleg.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.adapters.Playlist_Adapter;
import com.wolfsoft.oleg.bean.PlayListData;
import com.wolfsoft.oleg.bean.SignUpResponse;
import com.wolfsoft.oleg.retrofit.Api;
import com.wolfsoft.oleg.utils.ConstantMember;
import com.wolfsoft.oleg.utils.DialogsUtils;
import com.wolfsoft.oleg.utils.NetworkConnection;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_PlayList_Create extends Fragment {

    Context context;
    LinearLayout linearLayout_Create_Playlist;
    EditText editText_Playlist_Name;
    String str_title,headers,str_user_playlist;

    Handler handler = new Handler();
    RecyclerView recyclerView;

    List<PlayListData> playListData = new ArrayList<>();
    Playlist_Adapter adapter;
    Gson gson = new Gson();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment__play_list__create, container, false);

        context = getActivity();
        headers =ConstantMember.getHeader(context);

        linearLayout_Create_Playlist = view.findViewById(R.id.linear_create_playlist);
        linearLayout_Create_Playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkConnection.checkConnection(context)) {
                   showDialog(context);
                } else {
                    ConstantMember.showMessage(context,"No internet connection");
                }
            }
        });

        recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        getUserPlaylist();

        return view;
    }

    public void showDialog(Context activity){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_box_create_playlist);

        editText_Playlist_Name = dialog.findViewById(R.id.edit_box_playlist_name);
        Button dialogButton,button_Cancel;

        dialogButton = dialog.findViewById(R.id.btn_submit);
        button_Cancel = dialog.findViewById(R.id.btn_cancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_title = editText_Playlist_Name.getText().toString();
                if (str_title.length() > 0) {
                      apiCreatePlaylist();
                      dialog.dismiss();
                } else {
                    editText_Playlist_Name.setError("Please enter name");
                }
            }
        });

        button_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void apiCreatePlaylist() {
        DialogsUtils.showProgressDialog(context,"Please wait");
        Api.getClient().createUserPlayList(headers,str_title).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        ConstantMember.showMessage(context,""+response.body().getMessage());

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogsUtils.dismissDialog();
                                getUserPlaylist();
                            }
                        },1000);

                    } else {
                        ConstantMember.showMessage(context,""+response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                DialogsUtils.dismissDialog();
                Log.e("error",""+t);
            }
        });
    }

    public void getUserPlaylist() {
        DialogsUtils.showProgressDialog(context,"Please wait...");
        Api.getClient().getUserPlayList(headers,"").enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        DialogsUtils.dismissDialog();
                        playListData = new ArrayList<>();
                        playListData = response.body().getPlayList();
                        Log.e("play list"," size= "+playListData.size());
                        str_user_playlist = gson.toJson(playListData);
                        //Set list date in session
                        ConstantMember.setValueInSession(context,"user_play_list",str_user_playlist);
                        adapter = new Playlist_Adapter(context,playListData);
                        recyclerView.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                Log.e("error","list= "+t);
                DialogsUtils.dismissDialog();
            }
        });

    }
}
