package com.wolfsoft.oleg.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wolfsoft.oleg.R;
import com.wolfsoft.oleg.adapters.Play_List_Detail_Adapter;
import com.wolfsoft.oleg.bean.Featured_Audio_Data;
import com.wolfsoft.oleg.bean.PlayListData;

import java.util.List;


public class PlayList_Detail_Fragment extends Fragment {

    Context context;
    RecyclerView recyclerViewPlayList;
    List<Featured_Audio_Data> dataList;
    Play_List_Detail_Adapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_play_list_detail, container, false);
        context = getActivity();

        recyclerViewPlayList = view.findViewById(R.id.recycler);
        recyclerViewPlayList.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        adapter = new Play_List_Detail_Adapter(context,dataList);
        recyclerViewPlayList.setAdapter(adapter);

        return view;
    }
}
