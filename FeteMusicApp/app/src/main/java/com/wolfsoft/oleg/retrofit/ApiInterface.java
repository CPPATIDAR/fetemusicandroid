package com.wolfsoft.oleg.retrofit;

import com.wolfsoft.oleg.bean.CountryList;
import com.wolfsoft.oleg.bean.FeaturedResponse;
import com.wolfsoft.oleg.bean.SignUpResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {
    //Add only Get type web service
    @GET("getCountries")
    Call<CountryList> getCountryList();


    //Add only Post type web service
    @FormUrlEncoded
    @POST("register")
    Call<SignUpResponse> registerUser(@Field("email") String email,
                                      @Field("name") String name,
                                      @Field("password") String password,
                                      @Field("age") String age,
                                      @Field("relationship_status") String relationship_status,
                                      @Field("favorite_soca_genre") String favorite_soca_genre,
                                      @Field("country_id") String country_id);

    @FormUrlEncoded
    @POST("login")
    Call<SignUpResponse> loginUser(@Field("email") String email,
                                   @Field("password") String password);

    @FormUrlEncoded
    @POST("forgetPassword")
    Call<SignUpResponse> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("updatePassword")
    Call<SignUpResponse> forgotUpdatePassword(@Field("user_id") String user_id,
                                              @Field("forget_token") String forget_token,
                                              @Field("password") String password,
                                              @Field("password_confirmation") String password_confirmation);

    @Multipart
    @POST("updateProfile")
    Call<SignUpResponse> updateProfile(@Header("Authorization") String header,
                                       @Part("name") RequestBody name,
                                       @Part("age") RequestBody age,
                                       @Part("relationship_status") RequestBody relationship_status,
                                       @Part("favorite_soca_genre") RequestBody favorite_soca_genre,
                                       @Part("country_id") RequestBody country_id,
                                       @Part("email") RequestBody email,
                                       @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("getFeatured")
    Call<FeaturedResponse> getFeaturedList(@Header("Authorization") String header,
                                           @Field("value") String value);


    @FormUrlEncoded
    @POST("createPlayList")
    Call<SignUpResponse> createUserPlayList(@Header("Authorization") String header,
                                            @Field("title") String title);

    @FormUrlEncoded
    @POST("getUserPlaylists")
    Call<SignUpResponse> getUserPlayList(@Header("Authorization") String header,
                                         @Field("value") String value);

    @FormUrlEncoded
    @POST("addToPlaylist")
    Call<SignUpResponse> addToPlayList(@Header("Authorization") String header,
                                       @Field("playlist_id") String playlist_id,
                                       @Field("media_id") String media_id);

    @FormUrlEncoded
    @POST("getPlaylistMedias")
    Call<SignUpResponse> getPlayListMedia(@Header("Authorization") String header,
                                          @Field("playlist_id") String playlist_id,
                                          @Field("media_type") String media_type,
                                          @Field("page") String page);

    @FormUrlEncoded
    @POST("updatePlayListName")
    Call<SignUpResponse> updateUserPlayList(@Header("Authorization") String header,
                                            @Field("playlist_id") String playlist_id,
                                            @Field("title") String title);

    @FormUrlEncoded
    @POST("addToFavorite")
    Call<SignUpResponse> addToFavorite(@Header("Authorization") String header,
                                       @Field("media_id") String media_id);

    @FormUrlEncoded
    @POST("getMedia")
    Call<SignUpResponse> getMedia(@Header("Authorization") String header,
                                  @Field("media_type") String media_type,
                                  @Field("is_favorite") String is_favorite,
                                  @Field("artist_id") String artist_id,
                                  @Field("page") String page);

    @FormUrlEncoded
    @POST("getMedia")
    Call<FeaturedResponse> getVideoMedia(@Header("Authorization") String header,
                                  @Field("media_type") String media_type,
                                  @Field("is_favorite") String is_favorite,
                                  @Field("artist_id") String artist_id,
                                  @Field("page") String page);

    @FormUrlEncoded
    @POST("search")
    Call<FeaturedResponse> getSearchResult(@Header("Authorization") String header,
                                         @Field("search_keyword") String search_keyword,
                                         @Field("page") String page);

}
