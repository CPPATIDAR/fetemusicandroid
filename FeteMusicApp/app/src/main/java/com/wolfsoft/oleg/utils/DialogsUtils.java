package com.wolfsoft.oleg.utils;

import android.app.ProgressDialog;
import android.content.Context;

public  class DialogsUtils {
    private static ProgressDialog m_Dialog;
    public static void showProgressDialog(Context context, String message){
        m_Dialog = new ProgressDialog(context);
        m_Dialog.setMessage(message);
        m_Dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        m_Dialog.setCancelable(false);
        m_Dialog.show();
    }

    public static void dismissDialog() {
         m_Dialog.dismiss();
    }
}
